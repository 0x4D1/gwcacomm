from os import sleep

import math
import random
import strutils
import times
import unicode
import winlean

import processes
import gwtypes
import gwconstants

randomize()

# =============================================================================
# Pipe Functions
# =============================================================================
proc openNamedPipe( pipeName: string ): Handle =
    ## Opens a named pipe and sets its state to 'Message Mode'
    ## Return: {Handle} - the opened pipe
    result = createFileW( pipeName.newWideCString, (GENERIC_READ or GENERIC_WRITE), 0, nil, 0x3, 0, 0 )

    # If we failed to open the pipe tell someone!
    if result == INVALID_HANDLE_VALUE: raise newException( SystemError, "Could not open pipe: " & pipeName )

    # Put the pipe in the correct mode, even though it should already be in it
    var PIPE_READMODE_MESSAGE = 0x2
    discard setNamedPipeHandleState( result, cast[ptr DWORD](PIPE_READMODE_MESSAGE.addr), nil, nil )

proc closeNamedPipe( hPipe: var Handle ): bool {.discardable.} =
    ## Closes an opened pipe
    ## Return: {bool} - Handle was closed properly
    result = closeHandle( hPipe ).bool
    hPipe = 0

# =============================================================================
# Server Object / Server Communication
# =============================================================================
type GWClient* = ref object
    pipe*          : Handle
    lastWriteSize* : int32
    pointers*      : tuple[
        pGameContext,
        pAgentArray,
        pMapID,
        pSkillConst,
        pSkillTimer,
        pPing: int32
    ]
    hMemory*       : Handle
    hDll*          : Handle

proc transaction[T,Y]( client: GWClient, inBuffer: var T, outBuffer: var Y ): bool {.discardable.} =
    ## Send data to the server and recieve some back!
    ## Check example functions to understand usage better
    ## Return: {bool} - Transaction was successful
    result = transactNamedPipe( client.pipe, inBuffer.addr.pointer, sizeof(inBuffer).int32,
                                outBuffer.addr.pointer, sizeof(outBuffer).int32,
                                client.lastWriteSize.addr, nil ).bool
    discard flushFileBuffers( client.pipe )

proc send[T]( client: GWClient, inBuffer: var T ): bool {.discardable.} =
    ## Send data to the server
    ## Return: {bool} - Data sent properly
    result = writeFile( client.pipe, inBuffer.addr.pointer, sizeof(inBuffer).int32, client.lastWriteSize.addr, nil ).bool
    discard flushFileBuffers( client.pipe )

proc scanForPointers( client: GWClient ) =
    var
        # Get the module location for the exe so we only scan the memory for the game
        hModule = getModuleHandle( client.hMemory, "Gw.exe" )
        mInfo = MODULEINFO()
    discard getModuleInformation( client.hMemory, hModule, mInfo.addr, sizeof(mInfo).int32 )

    # dump all the game's memory into a sequence so we can scan it
    var memory = dumpMemory( client.hMemory, mInfo )

    # Pattern from GWA2 for the skill timer
    client.pointers.pSkillTimer = readPointer[int32]( client.hMemory,
        mInfo.lpBaseOfDll + findPattern(
            memory,
            [0x85'u8,0xC9,0x74,0x15,0x8B,0xD6,0x2B,0xD1,0x83,0xFA,0x64],
            "xxxxxxxxxxx",
            -0x4'i32
        )
    )

    client.pointers.pPing = readPointer[int32]( client.hMemory,
        mInfo.lpBaseOfDll + findPattern(
            memory,
            [0x90'u8, 0x8D, 0x41, 0x24, 0x8B, 0x49, 0x18, 0x6A, 0x30],
            "xxxxxxxxx",
            -0x9'i32
        )
    )

# Returns an item from the array
proc getArrayItem*( client: GWClient, arr: gwArray, index: int32 ): arr.T =
    if index > arr.current_size:
        echo repr( arr )
        echo index * sizeof(arr.T).int32
        raise newException( IndexError, "[gwArray] Index out of bounds!" ) 

    discard readProcessMemory(
        client.hMemory,
        (arr.array_ptr + ( index * sizeof(arr.T).int32 )),
        result.addr,
        sizeof(result).int32,
        client.lastWriteSize.addr
    )

iterator arrayItems( client: GWClient, arr: gwArray ): arr.T {.inline.} = 
    var index = 0
    while index < arr.current_size:
        yield client.getArrayItem( arr, index.int32 )
        inc index

# =============================================================================
# Server Commands
# =============================================================================
proc Client*( pid: int32, dll: string = "GWCAclient.dll" ): GWClient =
    result = GWClient()

    result.hMemory = openProcess( PROCESS_ALL_ACCESS, false, pid )

    result.hDll = injectDLL( result.hMemory, dll )

    # Wait for the gwcacomm to load
    sleep(500)

    result.pipe = openNamedPipe( "\\\\.\\pipe\\GWComm_" & $pid )

    # Preform a heartbeat to make sure the server is running.
    var
        data = ( ServerCommand.HEARTBEAT.int32, )
        buffer: int32 = 0

    result.transaction( data, buffer )

    if buffer != 1: raise newException( SystemError, "Error communicating over the pipe!" )

    # Get the base pointers
    data = ( ServerCommand.REQUEST_BASE_POINTERS.int32, )
    var pointers: array[4, int32]
    result.transaction( data, pointers )

    # map the base pointers to our pointers
    result.pointers.pGameContext = pointers[0]
    result.pointers.pAgentArray  = pointers[1]
    result.pointers.pMapID       = pointers[2]
    result.pointers.pSkillConst  = pointers[3]

    # Get the extra pointers
    result.scanForPointers()

proc Close*( client: GWClient ) =
    # Not unloading propperly
    # var data = ( ServerCommand.UNLOAD_client.int32, )
    # client.send( data )
    client.pipe.closeNamedPipe()

    # Not ejecting propperly..
    # discard ejectDLL( client.hMemory, client.hDll )

    discard closeHandle( client.hMemory )

# =============================================================================
# Helper Functions
# =============================================================================
proc distance*( x1, y1, x2, y2: float ): float =
    return sqrt( ( x2 - x1 ).pow(2) + ( y2 - y1 ).pow(2) )

proc distance*( agent: Agent, x, y: float ): float =
    return sqrt( ( x - agent.X ).pow(2) + ( y - agent.Y ).pow(2) )

proc distance*( agent1, agent2: Agent ): float =
    return sqrt( ( agent2.X - agent1.X ).pow(2) + ( agent2.Y - agent1.Y ).pow(2) )

proc pseudoDistance*( x1, y1, x2, y2: float ): float =
    return ( x2 - x1 ).pow(2) + ( y2 - y1 ).pow(2)

proc pseudoDistance*( agent: Agent, x, y: float ): float =
    return ( x - agent.X ).pow(2) + ( y - agent.Y ).pow(2)

proc pseudoDistance*( agent1, agent2: Agent ): float =
    return ( agent2.X - agent1.X ).pow(2) + ( agent2.Y - agent1.Y ).pow(2)

proc randFloat*( lower, higher: float ): float =
    return random( higher - lower ) + lower

proc randInt*( lower, higher: int ): int =
    return random( (higher - lower) + 1 ) + lower

proc chatString( s: string ): array[137,Rune16] =
    ## Turn our string in to a unicode w_char compliant array
    ## Return: {array[137,Rune16]}
    result = (new array[0..136, Rune16])[] # Hacky way to initialize the array
    for index in 0..s.len:
        result[index] = s[index].Rune16
        if index >= 136:
            # the last character needs to be 0, null terminated strings..
            result[136] = '\x00'.Rune16
            return result

# =============================================================================
# Game Functions
# =============================================================================
# -------------------------------------
# Properties - Memory Reads
# -------------------------------------
proc Ping*( client: GWClient ): int32 = 
    return readPointer[int32]( client.hMemory, client.pointers.pPing )
    # discard readProcessMemory( client.hMemory, client.pointers.pPing, result.addr, sizeof(result).int32, client.lastWriteSize.addr )

proc MapID*( client: GWClient ): int32 =
    return readPointer[int32]( client.hMemory, client.pointers.pMapID )
    # discard readProcessMemory( client.hMemory, client.pointers.pMapID, result.addr, sizeof(result).int32, client.lastWriteSize.addr )

proc PlayerID*( client: GWClient ): int32 =
    return readPointer[int32]( client.hMemory, client.pointers.pAgentArray - 0x54'i32 )

proc TargetID*( client: GWClient ): int32 =
    return readPointer[int32]( client.hMemory, client.pointers.pAgentArray - 0x500'i32 )

proc PlayerCharName*( client: GWClient ): string =
    var name: array[20, uint16]
    discard readProcessMemory( client.hMemory, 0x00A2AE80'i32, name.addr, sizeof(name).int32, client.lastWriteSize.addr )

    result = ""
    for c in name: result.add( char(c) )

    result = result.strip( chars = {'\0'} )

proc IsMapLoaded*( client: GWClient ): bool =
    var
        data = ( ServerCommand.GET_MAP_LOADED.int32, )
        loaded: int32 = 0
    client.transaction( data, loaded )
    return loaded.bool

proc SkillTimer*( client: GWClient ): int32 =
    return readPointer[int32]( client.hMemory, client.pointers.pSkillTimer )

# -------------------------------------
# Packets
# -------------------------------------
proc sendPacket*( client: GWClient, aData: varargs[int32] ) =
    ## Send a packet to the server
    var lData: seq[int32] = ServerCommand.SEND_PACKET.int32 & @aData
    # We have to figgure out the length of our data manually here which is why we're not using `send`
    discard writeFile( client.pipe, lData[0].addr.pointer, (lData.len*4).int32, client.lastWriteSize.addr, nil )
    discard flushFileBuffers( client.pipe )

proc addHero*( client: GWClient, heroID: int32 ) =
    ## Add a hero to your team
    client.sendPacket( 0x8, 0x00000017, heroID )

proc sendChat*( client: GWClient, channel: char, message: string ) =
    ## Send a message to the chat!
    var data = ( ServerCommand.SEND_CHAT.int32, channel.Utf16Char, message.chatString )
    client.send( data )

proc pickupItem*( client: GWClient, itemID: int32 ) = 
    client.sendPacket( 0xC, 0x00000039, itemID, 0 )

proc pickupItem*( client: GWClient, item: Item ) =
    client.sendPacket( 0xC, 0x00000039, item.AgentId, 0 )

proc setMode*( client: GWClient, difficulty: Difficulty ) =
    client.sendPacket( 0x8, 0x00000095, difficulty.int32 )

proc displayTitle*( client: GWClient, title: Titles ) =
    if title == Titles.None:
        client.sendPacket( 0x4, 0x52 )
    else:
        client.sendPacket( 0x8, 0x51, title.int32 )

proc attack*( client: GWClient, agent: Agent, callTarget: bool = false ) =
    client.sendPacket( 0xC, 0x00000020, agent.ID, callTarget.int32 )

proc dialogSelect*( client: GWClient, dialogID: int32 ) =
    client.sendPacket( 0x8, 0x00000035, dialogID )

proc takeQuest*( client: GWClient, questID: int32 ) =
    client.sendPacket( 0x8, 0x00000035, ("0x008" & toHex(questID, 3) & "01").parseHexInt.int32 )

proc acceptQuestReward*( client: GWClient, questID: int32 ) =
    client.sendPacket( 0x8, 0x00000035, ("0x008" & toHex(questID, 3) & "07").parseHexInt.int32 )

proc abandonQuest*( client: GWClient, questID: int32 ) =
    client.sendPacket( 0x8, 0x0000000A, questID )

proc interactNPC*( client: GWClient, npc: Agent, callTarget: bool = false ) =
    client.sendPacket( 0xC, 0x00000033, npc.ID, callTarget.int32 )

# -------------------------------------
# Actions
# -------------------------------------
proc performAction( client: GWClient, action, flag: int32, wait: bool = true ) =
    var data = ( ServerCommand.USE_TARGET_FUNC.int32, action, flag )
    client.send( data )
    if wait: sleep( client.Ping + 25 ) # wait for the action to be performed!

proc clearTarget*              ( client: GWClient ) = client.performAction(0xE3, 0x18)
proc targetNearestEnemy*       ( client: GWClient ) = client.performAction(0x93, 0x18)
proc targetNextEnemy*          ( client: GWClient ) = client.performAction(0x95, 0x18)
proc targetPreviousEnemy*      ( client: GWClient ) = client.performAction(0x9E, 0x18)
proc targetCalledTarget*       ( client: GWClient ) = client.performAction(0x9F, 0x18)
proc targetSelf*               ( client: GWClient ) = client.performAction(0xA0, 0x18)
proc targetNearestAlly*        ( client: GWClient ) = client.performAction(0xBC, 0x18)
proc targetNearestItem*        ( client: GWClient ) = client.performAction(0xC3, 0x18)
proc targetNextItem*           ( client: GWClient ) = client.performAction(0xC4, 0x18)
proc targetPreviousItem*       ( client: GWClient ) = client.performAction(0xC5, 0x18)
proc targetNextPartyMember*    ( client: GWClient ) = client.performAction(0xCA, 0x18)
proc targetPreviousPartyMember*( client: GWClient ) = client.performAction(0xCB, 0x18)
proc actionInteract*           ( client: GWClient ) = client.performAction(0x80, 0x18)
proc actionFollow*             ( client: GWClient ) = client.performAction(0xCC, 0x18)
proc dropBundle*               ( client: GWClient ) = client.performAction(0xCD, 0x18)
proc clearPartyCommands*       ( client: GWClient ) = client.performAction(0xDB, 0x18)
proc closeAllPanels*           ( client: GWClient ) = client.performAction(0x85, 0x18)
proc toggleHeroWindow*         ( client: GWClient ) = client.performAction(0x8A, 0x18)
proc toggleInventory*          ( client: GWClient ) = client.performAction(0x8B, 0x18)
proc toggleAllBags*            ( client: GWClient ) = client.performAction(0xB8, 0x18)
proc toggleWorldMap*           ( client: GWClient ) = client.performAction(0x8C, 0x18)
proc toggleOptions*            ( client: GWClient ) = client.performAction(0x8D, 0x18)
proc toggleQuestWindow*        ( client: GWClient ) = client.performAction(0x8E, 0x18)
proc toggleSkillWindow*        ( client: GWClient ) = client.performAction(0x8F, 0x18)
proc toggleMissionMap*         ( client: GWClient ) = client.performAction(0xB6, 0x18)
proc toggleFriendList*         ( client: GWClient ) = client.performAction(0xB9, 0x18)
proc toggleGuildWindow*        ( client: GWClient ) = client.performAction(0xBA, 0x18)
proc togglePartyWindow*        ( client: GWClient ) = client.performAction(0xBF, 0x18)
proc toggleScoreChart*         ( client: GWClient ) = client.performAction(0xBD, 0x18)
proc toggleLayoutWindow*       ( client: GWClient ) = client.performAction(0xC1, 0x18)
proc toggleMinionList*         ( client: GWClient ) = client.performAction(0xC2, 0x18)

proc changeWeaponSet*( client: GWClient, setNum: int32 ) = client.performAction(0x80 + setNum, 0x18)

proc displayAllies*( client: GWClient, display: bool ) =
    if display: client.performAction(0x89, 0x18)
    else:       client.performAction(0x89, 0x1A)

proc displayEnemies*( client: GWClient, display: bool ) =
    if display: client.performAction(0x94, 0x18)
    else:       client.performAction(0x94, 0x1A)

# -------------------------------------
# Skillbar
# -------------------------------------
proc getSkillRecharge*( client: GWClient, skill: SkillbarSkill ): int32 =
    if skill.Recharge == 0: return 0
    return skill.Recharge - client.SkillTimer()

proc isSkillRecharged*( client: GWClient, skill: SkillbarSkill ): bool =
    return client.getSkillRecharge( skill ) <= 0

proc getSkillbar*( client: GWClient, id: int32 ): Skillbar =
    ## For the player id = 0
    ## For hero id = hero slot #
    if id < 0 or id > 7: raise newException( Exception, "[getSkillbar] ID out of range!" )

    return readPointerChain[Skillbar](
        client.hMemory,
        client.pointers.pGameContext,
        [ 0x2C'i32, 0x6F0 ], # Context -> World -> SkillbarArray
        Skillbar.sizeof.int32 * id
    )

proc getPlayerSkillBar*( client: GWClient ): Skillbar = 
    return client.getSkillbar( 0 )

proc getPlayerSkills*( client: GWClient ): array[8, SkillbarSkill] = 
    return client.getSkillbar( 0 ).Skills

proc useSkill*( client: GWClient, slot: int32, targetID: int32 = 0, callTarget: bool = false ) =
    var data = ( ServerCommand.USE_SKILL.int32, slot - 1, targetID, callTarget.int32 )
    client.send( data )

proc useSkill*( client: GWClient, slot: int32, target: Agent, callTarget: bool = false ) =
    var data = ( ServerCommand.USE_SKILL.int32, slot - 1, target.ID, callTarget.int32 )
    client.send( data )

# -------------------------------------
# Agents
# -------------------------------------
proc getAgentArray( client: GWClient ): seq[Agent] =
    ## Returns a sequence of all agents loaded in memory.

    var gwAgentArray: gwArray[int32]
    discard readProcessMemory( client.hMemory, client.pointers.pAgentArray, gwAgentArray.addr, sizeof(gwAgentArray).int32, client.lastWriteSize.addr )

    result = newSeq[Agent]()

    var
        agentAddress: int32
        agent: Agent

    for index in 0..gwAgentArray.current_size:
        discard readProcessMemory( client.hMemory, gwAgentArray[index], agentAddress.addr, sizeof(agentAddress).int32, client.lastWriteSize.addr )
        if agentAddress == 0: continue

        discard readProcessMemory( client.hMemory, agentAddress, agent.addr, sizeof(agent).int32, client.lastWriteSize.addr )        
        result.add( agent )

proc getAgentsOfType*( client: GWClient, agentType: AgentType ): seq[Agent] =
    result = newSeq[Agent]()
    for agent in client.getAgentArray():
        if agent.Type == agentType.int32: result.add( agent )

proc getAgentsOfAllegiance*( client: GWClient, allegiance: Allegiance ): seq[Agent] =
    result = newSeq[Agent]()
    for agent in client.getAgentArray():
        if agent.Allegiance == allegiance.byte: result.add( agent )

proc getAgentsOfTypeAndAllegiance*( client: GWClient, agentType: AgentType, allegiance: Allegiance ): seq[Agent] =
    result = newSeq[Agent]()
    for agent in client.getAgentArray:
        if agent.Type == agentType.int32 and agent.Allegiance == allegiance.byte:
            result.add( agent )

# -------------------------------------
# Agent Allies
# -------------------------------------
proc getAllNPCs*( client: GWClient ): seq[Agent] =
    result = client.getAgentsOfTypeAndAllegiance( AgentType.Agent, Allegiance.Npc )

proc getNearestNPCCoords*( client: GWClient, x, y: float ): Agent =
    var
        currDistance = 0.0
        lastDistance = 500_000.0

    for npc in client.getAllNPCs():
        currDistance = pseudoDistance( npc, x, y )
        if currDistance < lastDistance:
            result = npc
            lastDistance = currDistance

# -------------------------------------
# Items
# -------------------------------------
proc getItemFromAgent( client: GWClient, agent: Agent ): Item =

    # Get the itemarray from the game context
    var itemarray = readPointerChain[gwArray[int32]](
        client.hMemory,
        client.pointers.pGameContext,
        [ 0x40'i32, ], # Context -> Items -> ItemArray
        0xB8
    )

    discard readProcessMemory(
        client.hMemory,
        readPointer[int32]( client.hMemory, itemarray[agent.ItemId] ),
        result.addr,
        sizeof(result).int32,
        client.lastWriteSize.addr
    )

proc getAllItemsOnGround*( client: GWClient ): seq[Item] =
    result = newSeq[Item]()

    for agent in client.getAgentsOfType( AgentType.Item ):
        result.add( client.getItemFromAgent( agent ) )

proc getItemRarity*( client: GWClient, item: Item ): ItemRarity =
    var rarity: uint16

    discard readProcessMemory( client.hMemory, item.ExtraItemInfo, rarity.addr, sizeof(rarity).int32, client.lastWriteSize.addr )

    return ItemRarity(rarity)

# -------------------------------------
# Bags
# -------------------------------------
proc getBag*( client: GWClient, bagid: BagID ): Bag =
    var bagPointer = readPointerChain[int32](
        client.hMemory,
        client.pointers.pGameContext,
        [ 0x40'i32, 0xF8 ], # Context -> Items -> Inventory
        (bagid.int32 * sizeof(int32)).int32
    )

    discard readProcessMemory( client.hMemory, bagPointer, result.addr, sizeof(result).int32, client.lastWriteSize.addr)

    # 0x40 -> 0xF8 -> bag# * sizeof(Bag)

proc getBagItems*( client: GWClient, bagid: BagID ): seq[Item] =
    var
        bag = client.getBag( bagid )
        itemPointer: int32
        item: Item

    result = newSeq[Item]()

    for i in 0..bag.Items.current_size-1:
        discard readProcessMemory( client.hMemory, bag.Items.array_ptr + i * sizeof(int32).int32, itemPointer.addr, sizeof(int32).int32, client.lastWriteSize.addr )

        if itemPointer == 0: continue

        discard readProcessMemory( client.hMemory, itemPointer, item.addr, sizeof(item).int32, client.lastWriteSize.addr )

        result.add(item)


# -------------------------------------
# Agent Information
# -------------------------------------
proc getAgentInfo*( client: GWClient, id: int32 ): Agent =
    var agentArray: gwArray[int32]

    # Load the guild wars array object
    discard readProcessMemory( client.hMemory, client.pointers.pAgentArray, agentArray.addr, sizeof(agentArray).int32, client.lastWriteSize.addr )

    # Get the address to the agent based upon his id
    var agentAddress = client.getArrayItem( agentArray, id )

    if agentAddress == 0: return # will return the default Agent object
        # return Agent( ID: -1 )
        # raise newException( IndexError, "[getAgentInfo] Invalid Agent ID" )

    # # Load the agent's info
    discard readProcessMemory( client.hMemory, agentAddress, result.addr, sizeof(result).int32, client.lastWriteSize.addr )    

proc getPlayer*( client: GWClient ): Agent =
    return client.getAgentInfo( client.PlayerID )

proc getTarget*( client: GWClient ): Agent =
    if client.TargetID > 0: return client.getAgentInfo( client.TargetID )

# -------------------------------------
# Effects
# -------------------------------------
proc getEffects*( client: GWClient, id: int32 ): AgentEffects =
    ## For the player id = 0
    ## For hero id = hero slot #
    if id < 0 or id > 7: raise newException( Exception, "[getEffects] ID out of range!" )

    result = readPointerChain[AgentEffects](
        client.hMemory,
        client.pointers.pGameContext,
        [ 0x2C'i32, 0x508 ], # Context -> World -> AgentEffectsArray
        AgentEffects.sizeof.int32 * id
    )

proc getEffectRemainingTime*( client: GWClient, effect: Effect ): int32 =
    if effect.Duration > 0:
        return ((effect.Duration * 1000.0).int32 - (client.SkillTimer() - effect.TimeStamp))  

proc getSkillRemainingTime*( client: GWClient, skill: SkillID, id: int32 = 0 ): int32 =
    for effect in client.arrayItems(client.getEffects( id ).Effects):
        if effect.SkillId != skill.int32: continue
        return client.getEffectRemainingTime( effect )

# -------------------------------------
# Travel
# -------------------------------------
proc travel*( client: GWClient, mapid, district: int32, region: MapRegion, language: MapLanguage ) =
    var data = ( ServerCommand.MAP_TRAVEL.int32, mapid, district, region.int32, language.int32 )
    client.send( data )

proc travelRandom*( client: GWClient, mapid: int32 ) =
    var data = ( ServerCommand.MAP_TRAVEL_RANDOM.int32, mapid )
    client.send( data )

proc waitMapLoaded*( client:GWClient, mapid: int32 ) =
    while client.MapId != mapid or not client.IsMapLoaded:
        sleep(200)
    sleep(4000)

proc travelTo*( client: GWClient, mapid: int32, region: MapRegion = MapRegion.American, language: MapLanguage = MapLanguage.English ) =
    client.travel( mapid, 0, MapRegion.American, MapLanguage.English )
    sleep(200)
    client.waitMapLoaded( mapid )

proc travelToRandom*( client: GWClient, mapid: int32 ) =
    client.travelRandom( mapid )
    sleep(200)
    client.waitMapLoaded( mapid )

# -------------------------------------
# Move
# -------------------------------------
proc move*( client: GWClient, x, y: float32, zplane: int32 = 0 ) =
    var data = ( ServerCommand.MOVE.int32, x, y, zplane )
    client.send( data )

proc moveRandom*( client: GWClient, x, y: float32, rand: float = 50 ) =
    client.move( x + randFloat( -rand, rand ), y + randFloat( -rand, rand ) )

proc moveTo*( client: GWClient, x, y: float32, rand: float = 50, distance: float = 100 ): bool {.discardable.} =
    var
        destX = x + randFloat( -rand, rand )
        destY = y + randFloat( -rand, rand )
        player  = client.getPlayer()
        blocked = 0

    client.move( destX, destY, 0 )

    while player.distance( destX, destY ) > distance and blocked < 10:
        sleep(200)
        player = client.getPlayer()

        if player.Hp <= 0: return false

        if player.MoveX == 0 and player.MoveY == 0:
            # echo "Stuck?"
            inc blocked

            # Doesn't seem to do much anyways?
            # destX = x + randFloat( -rand, rand )
            # destY = y + randFloat( -rand, rand )

            client.move( destX, destY )

    if blocked >= 10: return false
    return true

proc moveNPC*( client: GWClient, npc: Agent ) =
    client.moveTo( npc.X, npc.Y, distance = 300 )

    client.interactNPC( npc )

    sleep( client.Ping + 200 )

# =============================================================================
# Utility
# =============================================================================
proc getGuildWarsWindows*: seq[tuple[pid:int32, character:string]] =
    var client: GWClient
    result = newSeq[tuple[pid:int32, character:string]]()
    for pid in getAllPidsFromProcessName( "Gw.exe" ):
        # Make a dummy client that only has a handle for the memory as that's all we
        # neet to read the character name
        client = GWClient( hMemory: openProcess( PROCESS_ALL_ACCESS, false, pid ) )
        result.add(( pid: pid, character:client.PlayerCharName ))
        discard closeHandle( client.hMemory )

# =============================================================================
# Testing
# =============================================================================
when isMainModule:
    var
        pid    = processes.getPidFromProcessName( "Gw.exe" )
        client = Client( pid )
    #     player = client.getPlayer()

    var MadKingSteward = client.getNearestNPCCoords( 5329.55, 9042.48 )

    echo client.TargetID
    echo MadKingSteward.Id

    client.moveNPC( client.getTarget() )
    sleep(1000)

    for i in 0..150:
        client.takeQuest( 1123 )
        sleep(500)
        client.abandonQuest( 1123 )
        sleep(500)

    # echo repr( client.getBag( BagID.Backpack ) )

    # echo client.getBagItems( BagID.Backpack ).len

    # # echo client.MapId

    # echo player.X
    # echo player.Y

    # echo toHex(client.pointers.pGameContext)

    # var effects = client.getEffects( 0 )
    # echo repr( effects )

    # for effect in client.arrayItems(effects.Effects):
    #     # echo repr(effect)
    #     echo SkillID(effect.SkillId)

    #     echo client.getEffectRemainingTime( effect )

    # client.targetNearestAlly()
    
    # var target = client.getTarget()

    # client.moveNPC( target )

    # client.dialogSelect( 132 )

    # client.targetNearestEnemy()

    # echo client.TargetID

    # echo repr(target)

    # echo target.ID

    # client.travelToRandom( 650 )
    # client.useSkill( 1 )

    # client.sendPacket( 0x8, 0x51, 0x26 ) # Set Asura title

    # for item in client.getAllItemsOnGround():
    #     echo item.ModelId
    #     echo item.ExtraId

    # echo repr(client.getItemFromAgent( items[0] ))

    # var ItemsToKeep = [
    #     28434, # ToT Bags
    #     21797, # Mesmer Tome
    # ]

    # for item in client.getAllItemsOnGround():
    #     echo item.ModelId, " : ", item.ExtraId

    #     if item.ModelId in ItemsToKeep or
    #        client.getItemRarity( item ) == ItemRarity.Gold or
    #        (item.ModelId == 146 and item.ExtraID in [10,12]):
    #         client.pickupItem( item )
    #         sleep( (1000.0 * randFloat( 1.0, 1.5 )).int )

    # var skillbar = client.getPlayerSkillBar()

    # echo $( client.getSkillRecharge( skillbar.Skills[1] ) / 1000 )

    # client.moveTo( (target.X + player.X)/2, (target.Y + player.Y)/2 )

    # client.sendChat( '/', "clap" )

    client.Close()
