from os import sleep
import strutils
import times

import gui
import GWClient
import processes
import gwtypes
import gwconstants

# =============================================================================
# GUI Initialization
# =============================================================================
var
    window       = newGTKWindow( "Vaettir Farmer", 300, 200 )
    lblRuns      = label_new( "0" )
    lblDeaths    = label_new( "0" )
    lblStatus    = statusbar_new()
    btnToggleBot = button_new( "Start Bot" )
    btnRefresh   = button_new( "Refresh" )
    comboChars   = combo_box_text_new()

window.addWidget( vbox_new( false, 5 ) )
window    .addWidget( alignment_new(0,0,1,0) )
window        .addWidget( vbox_new( false, 5 ) )
window            .addWidget( hbox_new( false, 5 ) )
window                .addWidget( alignment_new(0,0,1,0) )
window                    .inlineWidget( comboChars )
window                    .popWidget()
window                .addWidget( alignment_new(0,0,0,0) )
window                    .addWidget( hbox_new( false, 5 ) )
window                        .inlineWidget( btnRefresh )
window                        .inlineWidget( btnToggleBot )
window                        .popWidget(3)
                   # Map ID Label
window            .addWidget( alignment_new(0,0,0,0) )
window                .addWidget( hbox_new( false, 5 ) )
window                    .inlineWidget( label_new( "Runs: " ) )
window                    .inlineWidget( lblRuns )
window                    .popWidget(2) # Pop up two levels
                                    # Map Loaded Label
window            .addWidget( alignment_new(0,0,0,0) )
window                .addWidget( hbox_new( false, 5 ) )
window                    .inlineWidget( label_new( "Deaths: " ) )
window                    .inlineWidget( lblDeaths )
window                    .popWidget(4)
          # Filler vbox to take all the vertical stretch
window    .addWidget( alignment_new(0,0,1,1) )
window        .inlineWidget( vbox_new( false, 5 ) )
window        .popWidget()
          # Status bar at the bottom
window    .addWidget( alignment_new(1,1,1,0) )
window        .addWidget( lblStatus )

for gw in getGuildWarsWindows(): comboChars.append_text( gw.character )

# =============================================================================
# Bot Code
# =============================================================================
type VaettirBot = ref object
    cin      : ptr Channel[string]
    cout     : ptr Channel[string]
    running  : bool
    started  : bool
    character: string
    client   : GWClient

proc send( bot: VaettirBot, message: string ) =
    if bot.cout == nil: return

    bot.cout[].send( message )

proc processMessages( bot: VaettirBot ) =
    if bot.cin == nil: return

    while bot.cin[].peek() > 0:
        var
            message = bot.cin[].recv()
            items   = message.split(':')

        case items[0]:
        of "start":
            bot.started = true
            bot.character = items[1].strip

        of "stop" : bot.started = false

        of "quit" :
            bot.started = false
            bot.running = false
            bot.cout[].close()
            bot.cin[].close()
            break

        else: continue

proc paused( bot: VaettirBot ): bool =
    bot.processMessages()
    if bot.started == false: return true
    return false

type VaettirBuild = enum
    DeadlyParadox    = ( 1, "Deadly Paradox" )
    ShadowForm       = ( 2, "Shadow Form" )
    ShroudOfDistress = ( 3, "Shroud Of Distress" )
    WayOfPerfection  = ( 4, "Way Of Perfection" )
    HeartOfShadow    = ( 5, "Heart Of Shadow" )
    WastrelsDemise   = ( 6, "Wastrel's Demise" )
    ArcaneEcho       = ( 7, "Arcane Echo" )
    Channeling       = ( 8, "Channeling" )

const
    ItemsToKeep = [
        2511,  # Gold
        28434, # ToT Bags
        21797, # Mesmer Tome
        27047, # Glacial Stone
        22751, # Lockpick
    ]

    # BjoraRunCoords: array[29, array[2, float]] = [
    BjoraRunCoords = [
        [  15003.8, -16598.1 ],
        [  15003.8, -16598.1 ],
        [  12699.5, -14589.8 ],
        [  11628.0, -13867.9 ],
        [  10891.5, -12989.5 ],
        [  10517.5, -11229.5 ],
        [  10209.1,  -9973.1 ],
        [   9296.5,  -8811.5 ],
        [   7815.6,  -7967.1 ],
        [   6266.7,  -6328.5 ],
        [   4940.0,  -4655.4 ],
        [   3867.8,  -2397.6 ],
        [   2279.6,  -1331.9 ],
        [      7.2,  -1072.6 ],
        [  -1752.7,  -1209.0 ],
        [  -3596.9,  -1671.8 ],
        [  -5386.6,  -1526.4 ],
        [  -6904.2,   -283.2 ],
        [  -7711.6,    364.9 ],
        [  -9537.8,   1265.4 ],
        [ -11141.2,    857.4 ],
        [ -12730.7,    371.5 ],
        [ -13379.0,     40.5 ],
        [ -14925.7,   1099.6 ],
        [ -16183.3,   2753.0 ],
        [ -17803.8,   4439.4 ],
        [ -18852.2,   5290.9 ],
        [ -19250.0,   5431.0 ],
        [ -19968.0,   5564.0 ]
    ]

    # VaettirLeftCoords: array[16, array[2, float]] = [
    VaettirLeftCoords = [
        [ 13501.0, -20925 ],
        [ 13172.0, -22137 ],
        [ 12496.0, -22600 ],
        [ 11375.0, -22761 ],
        [ 10925.0, -23466 ],
        [ 10917.0, -24311 ],
        [  9910.0, -24599 ],
        [  8995.0, -23177 ],
        [  8307.0, -23187 ],
        [  8213.0, -22829 ],
        [  8307.0, -23187 ],
        [  8213.0, -22829 ],
        [  8740.0, -22475 ],
        [  8880.0, -21384 ],
        [  8684.0, -20833 ],
        [  8982.0, -20576 ],
    ]

    # VaettirRightCoords: array[11, array[2, float]] = [
    VaettirRightCoords = [
        [ 10196.0, -20124 ],
        [  9976.0, -18338 ],
        [ 11316.0, -18056 ],
        [ 10392.0, -17512 ],
        [ 10114.0, -16948 ],
        [ 10729.0, -16273 ],
        [ 10810.0, -15058 ],
        [ 11120.0, -15105 ],
        [ 11670.0, -15457 ],
        [ 12604.0, -15320 ],
        [ 12476.0, -16157 ],
    ]

proc castSkills( bot: VaettirBot, aggroing: bool = true ) =
    if bot.paused: return

    bot.client.targetNearestEnemy()

    var
        client   = bot.client
        player   = client.getPlayer()
        target   = client.getTarget()
        skillBar = client.getPlayerSkills()

    if player.isDead(): return
    
    # if there is no target on the compass return
    if target.Id == 0: return

    # if we get are in range of a monster cast SF
    if distance( player, target ) <= Distance.Spellcast and
       player.energy > 20 and
       client.isSkillRecharged( skillBar[DeadlyParadox.int-1] ) and
       client.isSkillRecharged( skillBar[ShadowForm.int-1] ):

        client.useSkill( DeadlyParadox.int32 )
        sleep( client.Ping + Aftercast )
        if bot.paused: return

        client.useSkill( ShadowForm.int32 )
        sleep( client.Ping + 1000 + Aftercast )
        if bot.paused: return

    # if client.getSkillRemainingTime(SkillID.Shroud_of_Distress) < 10_000 and
    if player.HP < 0.8 and player.energy > 10 and client.isSkillRecharged( skillBar[ShroudOfDistress.int-1] ):
        client.useSkill( ShroudOfDistress.int32 )
        sleep( client.Ping + 1000 + Aftercast )
        if bot.paused: return

    if aggroing:
        # if client.getSkillRemainingTime(SkillID.Way_of_Perfection) < 10_000 and
        if player.energy > 5 and client.isSkillRecharged( skillBar[WayOfPerfection.int-1] ):
            client.useSkill( WayOfPerfection.int32 )
            sleep( client.Ping + 250 + Aftercast )
            if bot.paused: return

        if client.getSkillRemainingTime(SkillID.Channeling) < 5000 and
           player.energy > 5 and client.isSkillRecharged( skillBar[Channeling.int-1] ):
            client.useSkill( Channeling.int32 )
            sleep( client.Ping + 1000 + Aftercast )
            if bot.paused: return

    else:
        # if they're getting real close and we're low on hp use HOS to get away
        if player.HP < 0.4 and distance( player, target ) < 500 and
           player.energy > 5 and client.isSkillRecharged( skillBar[HeartOfShadow.int-1] ):
            client.useSkill( HeartOfShadow.int32 )
            sleep( client.Ping + 250 + Aftercast )
            if bot.paused: return

proc moveRunning( bot: VaettirBot, x, y: float, aggroing: bool = false, rand: float = 50 ): bool {.discardable.} =
    var
        client  = bot.client
        blocked = 0
        player: Agent
        skillBar: array[8, SkillbarSkill]

    client.moveRandom( x, y, rand )

    player = client.getPlayer()

    while distance( player, x, y ) > 250:
        if player.isDead: return false
        elif bot.paused: return true

        # Cast our skills!
        bot.castSkills( aggroing )

        player   = client.getPlayer()
        skillBar = client.getPlayerSkills()

        # If we are not moving, tell the character to move again, Also have a blocked counter
        if player.MoveX == 0 and player.MoveY == 0:
            if blocked > 5 and player.energy > 5 and client.isSkillRecharged( skillBar[HeartOfShadow.int] ):
                client.useSkill( HeartOfShadow.int32 )
                sleep( client.Ping + 250 + Aftercast )

            elif blocked > 15: return false

            client.moveRandom( x, y, 300 )
            blocked.inc

        sleep(100)

    return true

proc sleepWithSkills( bot: VaettirBot, seconds: float ) = 
    var startTime = epochTime()

    while epochTime() - startTime < seconds:
        bot.castSkills()
        sleep( 100 )

proc getGoodVaettirToHex( bot: VaettirBot ): Agent =
    var player = bot.client.getPlayer()
    for agent in bot.client.getAgentsOfType( AgentType.Agent ):
        if agent.Allegiance != Allegiance.Enemy.byte: continue
        if agent.HP <= 0: continue
        if distance( player, agent ) > Distance.Nearby: continue
        if agent.hasHex: continue
        if not agent.isEnchanted: continue

        return agent

proc killTheVaettir( bot: VaettirBot ): bool {.discardable.} =
    bot.client.targetNearestEnemy()
    var
        client = bot.client
        player = client.getPlayer()
        target = client.getTarget()
        skillBar: array[8, SkillbarSkill]

    while client.TargetID > 0 and target.HP > 0 and distance(player, target) < Distance.Area:
        client.targetNearestEnemy()
        target = client.getTarget()
        player = client.getPlayer()

        if player.isDead: return false
        elif bot.paused: return true

        bot.castSkills()

        skillBar = client.getPlayerSkills()

        if client.getSkillRemainingTime( SkillID.Shadow_Form ) > 7000 and
           skillBar[ArcaneEcho.int-1].SkillId == SkillID.Arcane_Echo.int32 and
           client.isSkillRecharged( skillBar[ArcaneEcho.int-1] ) and
           client.isSkillRecharged( skillBar[WastrelsDemise.int-1] ):
            client.useSkill( ArcaneEcho.int32 )
            sleep( client.Ping + 2000 + Aftercast )
            if bot.paused: return

            client.useSkill( WastrelsDemise.int32, bot.getGoodVaettirToHex() )
            sleep( client.Ping + 750 + Aftercast )
            if bot.paused: return

        if client.isSkillRecharged( skillBar[ArcaneEcho.int-1] ) and
           skillBar[ArcaneEcho.int-1].SkillId == SkillID.Wastrels_Demise.int32:
            client.useskill( ArcaneEcho.int32, bot.getGoodVaettirToHex() )
            sleep( client.Ping + 250 + Aftercast )
            if bot.paused: return

        if client.isSkillRecharged( skillBar[WastrelsDemise.int-1] ):
            client.useSkill( WastrelsDemise.int32, bot.getGoodVaettirToHex() )
            sleep( client.Ping + 250 + Aftercast )
            if bot.paused: return

        sleep( 50 )

proc botThread( chans: tuple[botOut, botIn: ptr Channel[string]] ) {.thread.} =
    var
        runs = 0
        deaths = 0
        client: GwClient
        bot = VaettirBot(
            cin    : chans.botIn,
            cout   : chans.botOut,
            running: true,
            started: false,
        )

    # run until we quit
    while bot.running:

        # Mark this location for when we are in another loop and need to exit out to the main loop
        block botLoop:

            if not bot.started: bot.send( "status: Waiting for you to start the bot." )

            # Wait until the bot is told to start
            while bot.running and not bot.started:
                bot.processMessages()
                sleep(200)

            # Get the guild wars process and open our client with it!
            if bot.client == nil:
                try:
                    # TODO Find all gw clients and get the character names have a way to choose which one to use.
                    # client = Client( getPidFromProcessName( "Gw.exe" ) )
                    for gw in getGuildWarsWindows():
                        if gw.character == bot.character:
                            client = Client( gw.pid )

                    if client.pipe == 0: raise newException( Exception, "" )

                    bot.client = client
                except:
                    bot.send( "status: [Error] Could not find an active Guild Wars client!" )
                    sleep( 3000 ) # sleep so they can see the error
                    bot.started = false
                    bot.send( "started: false" )
                    break botLoop

            # This allows us to go and wait for the player to start the bot again once he's paused it
            if bot.paused: break botLoop


            if client.MapID != 546:

                if client.MapID != 650: # Longeye's Ledge
                    bot.send( "status: Traveling to Longeye's Ledge." )
                    client.travelToRandom( 650 )

                if bot.paused: break botLoop
                
                client.setMode( Difficulty.Hard )

                bot.send( "status: Exiting outpost." )
                client.move( -26472, 16217 )
                client.waitMapLoaded( 482 ) # Bjora Marches
                
                if bot.paused: break botLoop

                client.displayTitle( Titles.Norn )

                bot.send( "status: Running through Bjora Marches..." )
                if not client.getPlayer.isDead():
                    for point in BjoraRunCoords:
                        if bot.paused: break botLoop
                        if not bot.moveRunning( point[0], point[1] ):
                            bot.send( "status: [Error] Didn't make it through Bjora Marches." )
                            break botLoop

                if bot.paused: break botLoop

                if not client.getPlayer.isDead():
                    bot.send( "status: Entering Jaga Moraine." )
                    client.move( -20076, 5580 )
                    client.waitMapLoaded( 546 ) # Jaga Moraine
            
            if bot.paused: break botLoop

            bot.send( "status: Taking Blessing." )
            client.targetNearestAlly()
            client.moveNPC( client.getTarget() )
            sleep(1000)
            client.dialogSelect( 132 ) # Take the blessing
            sleep(1200)

            bot.send( "status: Aggroing left side." )
            if not client.getPlayer.isDead():
                for point in VaettirLeftCoords:
                    if bot.paused: break botLoop
                    if not bot.moveRunning( point[0], point[1], true ):
                        bot.send( "status: [Error] Didn't make it through the left side!" )
                        sleep( 3000 ) # sleep so they can see the error
                        break botLoop


            bot.send( "status: Waiting for the left side to ball." )
            if not client.getPlayer.isDead(): bot.sleepWithSkills( 16 )

            if bot.paused: break botLoop

            bot.send( "status: Aggroing right side." )
            if not client.getPlayer.isDead():
                for point in VaettirRightCoords:
                    if bot.paused: break botLoop
                    if not bot.moveRunning( point[0], point[1], true ):
                        bot.send( "status: [Error] Didn't make it through the right side!" )
                        sleep( 3000 ) # sleep so they can see the error
                        break botLoop

            if bot.paused: break botLoop
            
            bot.send( "status: Waiting for the right side to ball." )
            if not client.getPlayer.isDead(): bot.sleepWithSkills( 16 )

            if bot.paused: break botLoop
            
            if not client.getPlayer.isDead():
                bot.send( "status: Blocking them in!" )
                bot.moveRunning( 12920, -17032, true, 30 )
                bot.moveRunning( 12847, -17136, true, 30 )
                bot.moveRunning( 12720, -17222, true, 30 )
                bot.sleepWithSkills( 0.4 )
                bot.moveRunning( 12617, -17273, true, 30 )
                bot.sleepWithSkills( 0.4 )
                bot.moveRunning( 12518, -17305, true, 20 )
                bot.sleepWithSkills( 0.4 )
                bot.moveRunning( 12464.7, -17330.5, true, 2 )

            if bot.paused: break botLoop
            if not client.getPlayer.isDead():
                bot.send( "status: Booooom!" )
                bot.killTheVaettir()

            if bot.paused: break botLoop
            sleep( 3000 )

            bot.send( "status: Collecting Items" )
            if not client.getPlayer.isDead():
                for item in client.getAllItemsOnGround():
                    if client.getPlayer.isDead(): break
                    if bot.paused: break botLoop
                    if item.ModelId in ItemsToKeep or
                       client.getItemRarity( item ) == ItemRarity.Gold or
                       (item.ModelId == 146 and item.ExtraID in [10,12]):
                        client.pickupItem( item )
                        sleep( (1000.0 * randFloat( 1.0, 1.5 )).int )

            sleep( 1000 )


            if client.getPlayer.isDead(): inc deaths
            while client.getPlayer.isDead(): sleep(500)
            bot.send( "deaths: " & $deaths )

            if bot.paused: break botLoop
            bot.send( "status: Walking to Bjora Marches" )
            bot.moveRunning( 12289, -17700 )
            bot.moveRunning( 15318, -20351 )

            if bot.paused: break botLoop
            bot.send( "status: Mapping to Bjora Marches" )
            client.move( 15865, -20531 )
            client.waitMapLoaded( 482 ) # Bjora Marches

            sleep( 1000 )

            if bot.paused: break botLoop
            bot.send( "status: Mapping to Jaga Moraine" )
            client.move( -20076, 5580 )
            client.waitMapLoaded( 546 ) # Jaga Moraine

            if bot.paused: break botLoop

            inc runs

            bot.send( "runs: " & $runs )

            if client.getBagItems( BagID.Backpack  ).len == 20 and
               client.getBagItems( BagID.BeltPouch ).len ==  5 and
               client.getBagItems( BagID.Bag1      ).len == 10:

                while not bot.paused:
                    bot.send( "status: Bot completed his job" )
                    sleep(1000)


# =============================================================================
# Bot thread setup
# =============================================================================
var
    thr: Thread[tuple[botOut, botIn: ptr Channel[string]]]
    botOut = cast[ptr Channel[string]](allocShared0(sizeof(Channel[string])))
    botIn  = cast[ptr Channel[string]](allocShared0(sizeof(Channel[string])))
    channels = (botOut, botIn)
    # Bool outside the thread to keep track of whether or not the bot has been started
    bStarted = false

botOut[].open()
botIn[].open()

createThread( thr, botThread, channels )

# =============================================================================
# Gui Update Timer
# =============================================================================
proc guiUpdate( data: gpointer ): gboolean {.cdecl.} =
    var
        chans = cast[ptr tuple[botOut, botIn: ptr Channel[string]]](data)[]
        # botIn  = chans.botIn
        botOut = chans.botOut

    while botOut[].peek() > 0:
        var
            message = botOut[].recv()
            items = message.split(':')

        if items.len == 0: return true

        case items[0]:
        of "status" : lblStatus.status = items[1].strip
        of "runs"   : lblRuns.set_text( items[1].strip )
        of "deaths" : lblDeaths.set_text( items[1].strip )
        of "started":
            bStarted = items[1].strip.parseBool
            if bStarted: btnToggleBot.set_label( "Stop Bot" )
            else:
                btnToggleBot.set_label( "Start Bot" )
                btnRefresh.set_sensitive(true)
                comboChars.set_sensitive(true)
        else: echo "Unknown Message: ", message

    return true

discard timeout_add( 200, guiUpdate, channels.addr )

# =============================================================================
# Gui Signals
# =============================================================================
#--------------------------------------
# Toggle Bot Button
#--------------------------------------
proc btnToggleBotClicked( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    if not bStarted:
        botIn[].send("start: " & $comboChars.get_active_text())
        btnToggleBot.set_label( "Stop Bot" )
        btnRefresh.set_sensitive(false)
        comboChars.set_sensitive(false)
        bStarted = true
    else:
        botIn[].send("stop")
        btnToggleBot.set_label( "Start Bot" )
        btnRefresh.set_sensitive(true)
        comboChars.set_sensitive(true)
        bStarted = false

discard btnToggleBot.signal_connect( "clicked", SIGNAL_FUNC(vaettir.btnToggleBotClicked), nil )

#--------------------------------------
# Refresh List Button
#--------------------------------------
proc btnRefreshClicked( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    if comboChars.WIDGET_SENSITIVE():
        for i in 0..10: comboChars.remove( 0.gint )
        for gw in getGuildWarsWindows(): comboChars.append_text( gw.character )
        
discard btnRefresh.signal_connect( "clicked", SIGNAL_FUNC(vaettir.btnRefreshClicked), nil )

#--------------------------------------
# Close button
#--------------------------------------
proc destroy( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    botIn[].send("quit")
    # botOut[].close()
    # botIn[].close()
    joinThreads( thr )

    main_quit()

discard window.mainWindow.signal_connect( "destroy", SIGNAL_FUNC(vaettir.destroy), nil )


# =============================================================================
# Gui start!
# =============================================================================
window.display()
