import glib2, gtk2

from gdk2 import TColor, PColor, color_parse

# Helper procs to set colors
proc set_bg*( widget: PWidget, scolor: string ) = 
    var color = TColor()
    discard color_parse( scolor, color.addr )
    widget.modify_bg( STATE_NORMAL, cast[PColor](color.addr) )

proc set_fg*( widget: PWidget, scolor: string ) = 
    var color = TColor()
    discard color_parse( scolor, color.addr )
    widget.modify_fg( STATE_NORMAL, cast[PColor](color.addr) )

# Helper for the statusbar
proc `status=`*(bar: PStatusbar, message: string) =
    discard bar.push( bar.get_context_id( message ), message )

type GTKWindow* = ref object
    mainWindow*: PWindow
    widgetTree: seq[PWidget]

proc newGTKWindow*( title: string, width, height: int32 ): GTKWindow =
    nim_init()

    result = GTKWindow()

    result.mainWindow = window_new( WINDOW_TOPLEVEL )
    result.mainWindow.set_title( title )
    result.mainWindow.set_default_size( width, height )
    result.mainWindow.set_position( WIN_POS_CENTER )
    result.mainWindow.set_border_width( 5 )

    result.widgetTree = newSeq[PWidget]()
    result.widgetTree.add( result.mainWindow )

proc display*( window: GTKWindow ) =
    window.widgetTree = newSeq[PWidget](0)
    window.mainWindow.show_all()
    main()

proc addWidget*( window: GTKWindow, widget: PWidget ): GTKWindow {.discardable.} =
    window.widgetTree[window.widgetTree.len-1].PContainer.add( widget )
    window.widgetTree.add( widget )
    return window

proc inlineWidget*( window: GTKWindow, widget: PWidget ): GTKWindow {.discardable.} =
    window.widgetTree[window.widgetTree.len-1].PContainer.add( widget )
    return window

proc popWidget*( window: GTKWindow, ammount = 1 ): GTKWindow {.discardable.} =
    if ammount < 1: raise newException( Exception, "[GTKWindow.pop] Invalid ammount!" )
    for i in 0..ammount-1: discard window.widgetTree.pop()
    return window

export glib2, gtk2