from os import sleep
import strutils
import times

import gui
import GWClient
import processes
import gwtypes
import gwconstants

# =============================================================================
# GUI Initialization
# =============================================================================
var
    window       = newGTKWindow( "Vaettir Farmer", 300, 200 )
    lblRuns      = label_new( "0" )
    lblLoading   = label_new( "False" )
    lblStatus    = statusbar_new()
    btnToggleBot = button_new( "Start Bot" )
    btnRefresh   = button_new( "Refresh" )
    comboChars   = combo_box_text_new()

window.addWidget( vbox_new( false, 5 ) )
window    .addWidget( alignment_new(0,0,1,0) )
window        .addWidget( vbox_new( false, 5 ) )
window            .addWidget( hbox_new( false, 5 ) )
window                .addWidget( alignment_new(0,0,1,0) )
window                    .inlineWidget( comboChars )
window                    .popWidget()
window                .addWidget( alignment_new(0,0,0,0) )
window                    .addWidget( hbox_new( false, 5 ) )
window                        .inlineWidget( btnRefresh )
window                        .inlineWidget( btnToggleBot )
window                        .popWidget(3)
                   # Map ID Label
window            .addWidget( alignment_new(0,0,0,0) )
window                .addWidget( hbox_new( false, 5 ) )
window                    .inlineWidget( label_new( "Runs: " ) )
window                    .inlineWidget( lblRuns )
window                    .popWidget(2) # Pop up two levels
                                    # Map Loaded Label
window            .addWidget( alignment_new(0,0,0,0) )
window                .addWidget( hbox_new( false, 5 ) )
window                    .inlineWidget( label_new( "Map Loaded: " ) )
window                    .inlineWidget( lblLoading )
window                    .popWidget(4)
          # Filler vbox to take all the vertical stretch
window    .addWidget( alignment_new(0,0,1,1) )
window        .inlineWidget( vbox_new( false, 5 ) )
window        .popWidget()
          # Status bar at the bottom
window    .addWidget( alignment_new(1,1,1,0) )
window        .addWidget( lblStatus )

for gw in getGuildWarsWindows(): comboChars.append_text( gw.character )

# =============================================================================
# Bot Code
# =============================================================================
type VaettirBot = ref object
    cin      : ptr Channel[string]
    cout     : ptr Channel[string]
    running  : bool
    started  : bool
    character: string
    client   : GWClient

proc send( bot: VaettirBot, message: string ) =
    bot.cout[].send( message )

proc processMessages( bot: VaettirBot ) =
    while bot.cin[].peek() > 0:
        var
            message = bot.cin[].recv()
            items   = message.split(':')

        case items[0]:
        of "start":
            bot.started = true
            bot.character = items[1].strip
        of "stop" : bot.started = false
        of "quit" : bot.started = false; bot.running = false
        else: continue

proc paused( bot: VaettirBot ): bool =
    bot.processMessages()
    if bot.started == false: return true
    return false

proc botThread( chans: tuple[botOut, botIn: ptr Channel[string]] ) {.thread.} =
    var
        runs = 0
        client: GwClient
        bot = VaettirBot(
            cin    : chans.botIn,
            cout   : chans.botOut,
            running: true,
            started: false,
        )

    # run until we quit
    while bot.running:

        # Mark this location for when we are in another loop and need to exit out to the main loop
        block botLoop:

            if not bot.started: bot.send( "status: Waiting for you to start the bot." )

            # Wait until the bot is told to start
            while bot.running and not bot.started:
                bot.processMessages()
                sleep(200)

            # Get the guild wars process and open our client with it!
            if bot.client == nil:
                try:
                    # TODO Find all gw clients and get the character names have a way to choose which one to use.
                    # client = Client( getPidFromProcessName( "Gw.exe" ) )
                    for gw in getGuildWarsWindows():
                        if gw.character == bot.character:
                            client = Client( gw.pid )

                    if client.pipe == 0: raise newException( Exception, "" )

                    bot.client = client
                except:
                    bot.send( "status: [Error] Could not find an active Guild Wars client!" )
                    sleep( 3000 ) # sleep so they can see the error
                    bot.started = false
                    bot.send( "started: false" )
                    break botLoop

            # This allows us to go and wait for the player to start the bot again once he's paused it
            if bot.paused: break botLoop

            var MadKingSteward = client.getNearestNPCCoords( 5329.55, 9042.48 )

            client.moveNPC( MadKingSteward )
            sleep(1000)

            for i in 0..150:
                client.takeQuest( 1123 )
                sleep(500)
                client.abandonQuest( 1123 )
                sleep(500)

            while not bot.paused:
                var player = client.getPlayer()
                bot.send( "status: X|" & $player.X & " Y|" & $player.Y )
                sleep(1000)

# =============================================================================
# Bot thread setup
# =============================================================================
var
    thr: Thread[tuple[botOut, botIn: ptr Channel[string]]]
    botOut = cast[ptr Channel[string]](allocShared0(sizeof(Channel[string])))
    botIn  = cast[ptr Channel[string]](allocShared0(sizeof(Channel[string])))
    channels = (botOut, botIn)
    # Bool outside the thread to keep track of whether or not the bot has been started
    bStarted = false

botOut[].open()
botIn[].open()

createThread( thr, botThread, channels )

# =============================================================================
# Gui Update Timer
# =============================================================================
proc guiUpdate( data: gpointer ): gboolean {.cdecl.} =
    var
        chans = cast[ptr tuple[botOut, botIn: ptr Channel[string]]](data)[]
        # botIn  = chans.botIn
        botOut = chans.botOut

    while botOut[].peek() > 0:
        var
            message = botOut[].recv()
            items = message.split(':')

        if items.len == 0: return true

        case items[0]:
        of "status" : lblStatus.status = items[1].strip
        of "runs"   : lblRuns.set_text( items[1].strip )
        of "loading": lblLoading.set_text( items[1].strip )
        of "started":
            bStarted = items[1].strip.parseBool
            if bStarted: btnToggleBot.set_label( "Stop Bot" )
            else:
                btnToggleBot.set_label( "Start Bot" )
                btnRefresh.set_sensitive(true)
                comboChars.set_sensitive(true)
        else: echo "Unknown Message: ", message

    return true

discard timeout_add( 200, guiUpdate, channels.addr )

# =============================================================================
# Gui Signals
# =============================================================================
#--------------------------------------
# Toggle Bot Button
#--------------------------------------
proc btnToggleBotClicked( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    if not bStarted:
        botIn[].send("start: " & $comboChars.get_active_text())
        btnToggleBot.set_label( "Stop Bot" )
        btnRefresh.set_sensitive(false)
        comboChars.set_sensitive(false)
        bStarted = true
    else:
        botIn[].send("stop")
        btnToggleBot.set_label( "Start Bot" )
        btnRefresh.set_sensitive(true)
        comboChars.set_sensitive(true)
        bStarted = false

discard btnToggleBot.signal_connect( "clicked", SIGNAL_FUNC(mobstoppergetter.btnToggleBotClicked), nil )

#--------------------------------------
# Refresh List Button
#--------------------------------------
proc btnRefreshClicked( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    if comboChars.WIDGET_SENSITIVE():
        for i in 0..10: comboChars.remove( 0.gint )
        for gw in getGuildWarsWindows(): comboChars.append_text( gw.character )
        
discard btnRefresh.signal_connect( "clicked", SIGNAL_FUNC(mobstoppergetter.btnRefreshClicked), nil )

#--------------------------------------
# Close button
#--------------------------------------
proc destroy( widget: PWidget, data: Pgpointer ) {.cdecl.} =
    botIn[].send("quit")
    joinThreads( thr )
    botOut[].close()
    botIn[].close()
    main_quit()

discard window.mainWindow.signal_connect( "destroy", SIGNAL_FUNC(mobstoppergetter.destroy), nil )


# =============================================================================
# Gui start!
# =============================================================================
window.display()
