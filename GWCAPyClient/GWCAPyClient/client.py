import os
from ctypes import * # Bad but doesn't affect performance nor use if he includes package

class GWCAClient(object):

	def __init__(self, pid):
		self.pipe = None

		try:
			self.connect('GWComm_%d' % pid)
		except FileNotFoundError:
			raise Exception('Make sure to open the server.') from None

	def __del__(self):
		if self.pipe:
			self.send(b'\x01\x00\x00\x00') # send disconnect, could actually put c_ulong(1)
			os.close(self.pipe)

	def connect(self, pipe_name):
		self.pipe = os.open('\\\\.\\pipe\\' + pipe_name, os.O_RDWR | os.O_BINARY)

	def receive(self, buffer):
		raw = os.read(self.pipe, sizeof(buffer))
		return buffer.from_buffer_copy(raw)

	def send(self, *args):
		data = b''.join(args)
		return os.write(self.pipe, data)

	def Hearthbeat(self):
		sent = self.send(b'\x00\x00\x00\x00') # could actually put c_ulong(0)
		receive = self.receive(c_ulong).value

		return (sent and receive)


class SimpleProcess(object):

	def __init__(self, pid, procname = None, window = None, classname = None):
		self.pid = pid
		self.procname = procname
		self.window = window
		self.classname = classname

	def __str__(self):
		return str('Process id   => %u\nProcess name => %s\nWindow name  => %s\nClass name   => %s' % \
					(self.pid, self.procname, self.window, self.classname))

	@classmethod
	def from_process_name(cls, name):
		pid = 0

		for _pid, pname in SimpleProcess.EnumProcess():
			if pname == name:
				pid = _pid

		return SimpleProcess(pid, name)

	@classmethod
	def from_window_name(cls, name):
		handle = windll.user32.FindWindowW(None, name)

		pid = c_ulong()
		classname = (c_wchar * 100)()

		windll.user32.GetWindowThreadProcessId(handle, byref(pid))
		windll.user32.GetClassNameW(handle, byref(classname), 100)
		pid = pid.value

		pname = None

		for _pid, _pname in SimpleProcess.EnumProcess():
			if pid == _pid:
				pname = _pname

		return SimpleProcess(pid, pname, name, classname.value)

	@classmethod
	def from_class_name(cls, name):
		pass

	@staticmethod
	def EnumProcess():
		hSnap = windll.kernel32.CreateToolhelp32Snapshot(0x2, 0x0) # Go trough process id
		buffer = (c_ubyte * 0x128)(0x28, 0x01, 0x00, 0x00) # We set the size quite sketchy
		name = (c_char * 260)

		windll.kernel32.Process32First(hSnap, byref(buffer))

		while windll.kernel32.Process32Next(hSnap, byref(buffer)):
			pid 	= c_ulong.from_buffer(buffer, 0x8).value
			pname 	= name.from_buffer(buffer, 0x24).value.decode('ascii')
			yield pid, pname

		windll.kernel32.CloseHandle(hSnap)

