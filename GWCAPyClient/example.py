import GWCAPyClient as client
from ctypes import (
	c_ulong as dword,
	c_float as float,
)

proc = client.SimpleProcess.from_process_name('Gw.exe')
inst = client.GWCAClient(proc.pid)
print(proc)
print('----------- Start -----------')
print('[Test]', '\n\tHearthbeat => ', inst.Hearthbeat())

inst.send(dword(9))
player = inst.receive(client.PseudoAgent)
print('[Player info]\n\tId => %d\n\tX  => %f\n\tY  => %f' % (player.Id, player.X, player.Y))

inst.receive(dword) # Remove sentinel

cmd, id = dword(10), dword(-1)
inst.send(cmd, id)
target = inst.receive(client.PseudoAgent)
print('[Target info]\n\tId => %d\n\tX  => %f\n\tY  => %f' % (target.Id, target.X, target.Y))

inst.receive(dword) # Remove sentinel

cmd, x, y, plane = dword(4), float((player.X + target.X) / 2), float((player.Y + target.Y) / 2), dword(0)
inst.send(cmd, x, y, plane)
