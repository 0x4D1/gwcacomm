#include-once

; GWCAComm _Quest_ Handlers
; Auto Generated


Func _Quest_id($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0000, "long*", 0x04)
EndFunc ; <== id

Func _Quest_LogState($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0004, "long*", 0x04)
EndFunc ; <== LogState

Func _Quest_unknown1($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0008, "byte[12]*", 0x0C)
EndFunc ; <== unknown1

Func _Quest_MapFrom($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0014, "long*", 0x04)
EndFunc ; <== MapFrom

Func _Quest_X($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0018, "float*", 0x04)
EndFunc ; <== X

Func _Quest_Y($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x001C, "float*", 0x04)
EndFunc ; <== Y

Func _Quest_unknown2($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0020, "uint64*", 0x08)
EndFunc ; <== unknown2

Func _Quest_MapTo($hProcess, $hQuest)
	Return _GWCA_Read($hProcess, $hQuest + 0x0028, "long*", 0x04)
EndFunc ; <== MapTo
