#include-once

; GWCAComm _Item_ Handlers
; Auto Generated


Func _Item_id($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0000, "long*", 0x04)
EndFunc ; <== id

Func _Item_agentId($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0004, "long*", 0x04)
EndFunc ; <== agentId

Func _Item_unknown1($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0008, "ptr*", 0x04)
EndFunc ; <== unknown1

Func _Item_bag($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x000C, "ptr*", 0x04)
EndFunc ; <== bag

Func _Item_modstruct($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0010, "ptr*", 0x04)
EndFunc ; <== modstruct

Func _Item_modstructsize($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0014, "long*", 0x04)
EndFunc ; <== modstructsize

Func _Item_customized($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0018, "ptr*", 0x04)
EndFunc ; <== customized

Func _Item_modelfileid($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x001C, "dword*", 0x04)
EndFunc ; <== modelfileid

Func _Item_unknown2b($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x001E, "word*", 0x02)
EndFunc ; <== unknown2b

Func _Item_type($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0020, "byte*", 0x01)
EndFunc ; <== type

Func _Item_unknown3($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0021, "byte*", 0x01)
EndFunc ; <== unknown3

Func _Item_extraId($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0022, "short*", 0x02)
EndFunc ; <== extraId

Func _Item_value($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0024, "short*", 0x02)
EndFunc ; <== value

Func _Item_unknown4($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0026, "word*", 0x02)
EndFunc ; <== unknown4

Func _Item_interaction($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0028, "dword*", 0x04)
EndFunc ; <== interaction

Func _Item_modelId($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x002C, "long*", 0x04)
EndFunc ; <== modelId

Func _Item_modString($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0030, "ptr*", 0x04)
EndFunc ; <== modString

Func _Item_unknown5($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0034, "dword*", 0x04)
EndFunc ; <== unknown5

Func _Item_NameString($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0038, "ptr*", 0x04)
EndFunc ; <== NameString

Func _Item_unknown6a($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x003C, "ptr*", 0x04)
EndFunc ; <== unknown6a

Func _Item_unknown6b($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0040, "dword*", 0x04)
EndFunc ; <== unknown6b

Func _Item_unknown6c($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0044, "dword*", 0x04)
EndFunc ; <== unknown6c

Func _Item_unknown6d($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x0048, "word*", 0x02)
EndFunc ; <== unknown6d

Func _Item_unknown6e($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x004A, "byte*", 0x01)
EndFunc ; <== unknown6e

Func _Item_quantity($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x004B, "byte*", 0x01)
EndFunc ; <== quantity

Func _Item_equipped($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x004C, "byte*", 0x01)
EndFunc ; <== equipped

Func _Item_profession($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x004D, "byte*", 0x01)
EndFunc ; <== profession

Func _Item_slot($hProcess, $hItem)
	Return _GWCA_Read($hProcess, $hItem + 0x004E, "byte*", 0x01)
EndFunc ; <== slot
