#include-once

; GWCAComm _Skillbar_ Handlers
; Auto Generated


Func _Skillbar_AgentId($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0000, "long*", 0x04)
EndFunc ; <== AgentId

Func _Skillbar_AdrenalineA1($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0004, "long*", 0x04)
EndFunc ; <== AdrenalineA1

Func _Skillbar_AdrenalineB1($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0008, "long*", 0x04)
EndFunc ; <== AdrenalineB1

Func _Skillbar_Recharge1($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x000C, "dword*", 0x04)
EndFunc ; <== Recharge1

Func _Skillbar_Id1($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0010, "dword*", 0x04)
EndFunc ; <== Id1

Func _Skillbar_Event1($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0014, "dword*", 0x04)
EndFunc ; <== Event1

Func _Skillbar_AdrenalineA2($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0018, "long*", 0x04)
EndFunc ; <== AdrenalineA2

Func _Skillbar_AdrenalineB2($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x001C, "long*", 0x04)
EndFunc ; <== AdrenalineB2

Func _Skillbar_Recharge2($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0020, "dword*", 0x04)
EndFunc ; <== Recharge2

Func _Skillbar_Id2($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0024, "dword*", 0x04)
EndFunc ; <== Id2

Func _Skillbar_Event2($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0028, "dword*", 0x04)
EndFunc ; <== Event2

Func _Skillbar_AdrenalineA3($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x002C, "long*", 0x04)
EndFunc ; <== AdrenalineA3

Func _Skillbar_AdrenalineB3($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0030, "long*", 0x04)
EndFunc ; <== AdrenalineB3

Func _Skillbar_Recharge3($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0034, "dword*", 0x04)
EndFunc ; <== Recharge3

Func _Skillbar_Id3($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0038, "dword*", 0x04)
EndFunc ; <== Id3

Func _Skillbar_Event3($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x003C, "dword*", 0x04)
EndFunc ; <== Event3

Func _Skillbar_AdrenalineA4($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0040, "long*", 0x04)
EndFunc ; <== AdrenalineA4

Func _Skillbar_AdrenalineB4($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0044, "long*", 0x04)
EndFunc ; <== AdrenalineB4

Func _Skillbar_Recharge4($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0048, "dword*", 0x04)
EndFunc ; <== Recharge4

Func _Skillbar_Id4($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x004C, "dword*", 0x04)
EndFunc ; <== Id4

Func _Skillbar_Event4($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0050, "dword*", 0x04)
EndFunc ; <== Event4

Func _Skillbar_AdrenalineA5($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0054, "long*", 0x04)
EndFunc ; <== AdrenalineA5

Func _Skillbar_AdrenalineB5($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0058, "long*", 0x04)
EndFunc ; <== AdrenalineB5

Func _Skillbar_Recharge5($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x005C, "dword*", 0x04)
EndFunc ; <== Recharge5

Func _Skillbar_Id5($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0060, "dword*", 0x04)
EndFunc ; <== Id5

Func _Skillbar_Event5($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0064, "dword*", 0x04)
EndFunc ; <== Event5

Func _Skillbar_AdrenalineA6($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0068, "long*", 0x04)
EndFunc ; <== AdrenalineA6

Func _Skillbar_AdrenalineB6($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x006C, "long*", 0x04)
EndFunc ; <== AdrenalineB6

Func _Skillbar_Recharge6($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0070, "dword*", 0x04)
EndFunc ; <== Recharge6

Func _Skillbar_Id6($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0074, "dword*", 0x04)
EndFunc ; <== Id6

Func _Skillbar_Event6($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0078, "dword*", 0x04)
EndFunc ; <== Event6

Func _Skillbar_AdrenalineA7($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x007C, "long*", 0x04)
EndFunc ; <== AdrenalineA7

Func _Skillbar_AdrenalineB7($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0080, "long*", 0x04)
EndFunc ; <== AdrenalineB7

Func _Skillbar_Recharge7($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0084, "dword*", 0x04)
EndFunc ; <== Recharge7

Func _Skillbar_Id7($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0088, "dword*", 0x04)
EndFunc ; <== Id7

Func _Skillbar_Event7($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x008C, "dword*", 0x04)
EndFunc ; <== Event7

Func _Skillbar_AdrenalineA8($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0090, "long*", 0x04)
EndFunc ; <== AdrenalineA8

Func _Skillbar_AdrenalineB8($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0094, "long*", 0x04)
EndFunc ; <== AdrenalineB8

Func _Skillbar_Recharge8($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x0098, "dword*", 0x04)
EndFunc ; <== Recharge8

Func _Skillbar_Id8($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x009C, "dword*", 0x04)
EndFunc ; <== Id8

Func _Skillbar_Event8($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x00A0, "dword*", 0x04)
EndFunc ; <== Event8

Func _Skillbar_disabled($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x00A4, "dword*", 0x04)
EndFunc ; <== disabled

Func _Skillbar_QueuePtr($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x00A8, "ptr*", 0x04)
EndFunc ; <== QueuePtr

Func _Skillbar_unknown($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x00AC, "dword*", 0x08)
EndFunc ; <== unknown

Func _Skillbar_Casting($hProcess, $hSkillbar)
	Return _GWCA_Read($hProcess, $hSkillbar + 0x00B0, "dword*", 0x04)
EndFunc ; <== Casting
