#include-once

; GWCAComm _Effect_ Handlers
; Auto Generated


Func _Effect_SkillId($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x0000, "long*", 0x04)
EndFunc ; <== SkillId

Func _Effect_EffectType($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x0004, "long*", 0x04)
EndFunc ; <== EffectType

Func _Effect_EffectId($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x0008, "long*", 0x04)
EndFunc ; <== EffectId

Func _Effect_AgentId($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x000C, "long*", 0x04)
EndFunc ; <== AgentId

Func _Effect_Duration($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x0010, "float*", 0x04)
EndFunc ; <== Duration

Func _Effect_TimeStamp($hProcess, $hEffect)
	Return _GWCA_Read($hProcess, $hEffect + 0x0014, "long*", 0x04)
EndFunc ; <== TimeStamp
