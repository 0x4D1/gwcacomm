#cs ----------------------------------------------------------------------------

							GWCAComm AutoIt Client


 AutoIt Version: 3.3.14.2
 Author:         4D 1

 Script Function:
	Client communicator for the GWCAComm dll. Used to communicate and interface
	with the Guild Wars client.

#ce ----------------------------------------------------------------------------

Const $NaN = +1.0 / 0.0

#Region Includes

#include '.\Handlers\Agent.au3'
#include '.\Handlers\Bag.au3'
#include '.\Handlers\Buff.au3'
#include '.\Handlers\Effect.au3'
#include '.\Handlers\Item.au3'
#include '.\Handlers\Quest.au3'
#include '.\Handlers\Skill.au3'
#include '.\Handlers\Skillbar.au3'

#include '.\Misc\Constants.au3'

#EndRegion Includes

#Region Constants
 Enum   $GWCA_HEARTBEAT,  _
		$GWCA_DISCONNECT_PIPE_CONNECTION,  _
		$GWCA_UNLOAD_SERVER, _
		$GWCA_REQUEST_BASE_POINTERS, _
		$GWCA_MOVE,  _
		$GWCA_USE_SKILL,  _
		$GWCA_GET_SKILL_RECHARGE, _
		$GWCA_GET_CAST_QUEUE_SIZE, _
		$GWCA_GET_SKILL_ADRENALINE, _
		$GWCA_GET_EFFECT_TIME_REMAINING, _
		$GWCA_SEND_CHAT, _
		$GWCA_GET_PLAYER_ID, _
		$GWCA_GET_TARGET_ID, _
		$GWCA_GET_PLAYER, _
		$GWCA_GET_AGENT_BY_ID, _
		$GWCA_GET_AGENTS_POS, _
		$GWCA_GET_AGENT_POS, _
		$GWCA_ALLOCATE_MEMORY, _
		$GWCA_FREE_MEMORY, _
		$GWCA_GET_MAP_ID, _
		$GWCA_GET_MAP_INFO, _
		$GWCA_GET_INSTANCE_TYPE, _
		$GWCA_GET_INSTANCE_TIME, _
		$GWCA_CHANGE_TARGET, _
		$GWCA_GO_NPC, _
		$GWCA_SEND_DIALOG, _
		$GWCA_MAP_TRAVEL, _
		$GWCA_MAP_TRAVEL_RANDOM, _
		$GWCA_TRAVEL_GUILD_HALL, _
		$GWCA_LEAVE_GUILD_HALL, _
		$GWCA_GET_MAP_LOADED, _
		$GWCA_CHANGE_SECONDARY, _
		$GWCA_SET_ATTRIBUTES, _
		$GWCA_LOAD_SKILLBAR, _
		$GWCA_SET_RENDER_HOOK, _
		$GWCA_SEND_PACKET, _
		$GWCA_GET_CHARACTER_NAME, _
		$GWCA_GET_AGENT_PTR, _
		$GWCA_GET_FILTERED_AGENT_ARRAY, _
		$GWCA_GET_ITEM_BY_SLOT, _
		$GWCA_GET_ITEM_BY_AGENTID, _
		$GWCA_GET_ITEMS_BY_MODELID, _
		$GWCA_GET_ITEM_NAME, _
		$GWCA_BUY_MERCHANT_ITEM, _
		$GWCA_SELL_MERCHANT_ITEM, _
		$GWCA_COLLECT_ITEM, _
		$GWCA_CRAFT_ITEM, _
		$GWCA_COLLECT_ITEM, _
		$GWCA_BUY_TRADER_ITEM, _
		$GWCA_SELL_TRADER_ITEM, _
		$GWCA_FIX_AGENT_POSITION, _
		$GWCA_GET_EFFECT_HANDLE, _
		$GWCA_GET_SKILLBAR_HANDLE, _
		$GWCA_GET_SKILLBAR_INFO
		$GWCA_COMMANDS_END
#EndRegion Constants



#Region Memory Base

Func _GWCA_OpenMemory($iProcessId)
	Return DllCall("kernel32.dll", "HANDLE", "OpenProcess", "DWORD", 0x1F0FFF, "BOOL", False, "DWORD", $iProcessId)[0]
EndFunc

Func _GWCA_CloseMemory($hProcess)
	Return DllCall("kernel32.dll", "BOOL", "CloseHandle", "HANDLE", $hProcess)[0]
EndFunc

Func _GWCA_Read($hProcess,$pAddress,$sType = 'DWORD*', $iSize = 4,$vBuffer = 0)
	Return DllCall("kernel32.dll", "BOOL", "ReadProcessMemory", "HANDLE", $hProcess, "PTR", $pAddress, $sType, $vBuffer, "DWORD", $iSize, "DWORD", 0)[3]
EndFunc

Func _GWCA_Write($hProcess,$pAddress,$vValue,$sType = 'DWORD*', $iSize = 4)
	Return DllCall("kernel32.dll", "BOOL", "WriteProcessMemory", "HANDLE", $hProcess, "PTR", $pAddress, $sType, $vValue, "DWORD", $iSize, "DWORD", 0)[0]
EndFunc

Func _GWCA_ReadOffsets($hProcess, $pAddress, $aOffset, $sType = 'DWORD*',$iSize = 4)
   Local $nPointerCount = $aOffset[0]
   Local Static $tBuffer = DllStructCreate('ptr')
   Local Static $pBuffer = DllStructGetPtr($tBuffer)

   For $i = 1 To $nPointerCount - 1
	  DllCall("kernel32.dll", 'int', 'ReadProcessMemory', 'handle', $hProcess, 'ptr', $pAddress + $aOffset[$i], 'ptr', $pBuffer, 'ulong_ptr', 4, 'ulong_ptr*', 0)
	  $pAddress = DllStructGetData($tBuffer, 1)
	  ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $pAddress = ' & $pAddress & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
	  If $pAddress = 0 Then
		 Local $lData[2] = [0, 0]
		 Return $lData
	  EndIf
   Next
   $pAddress += $aOffset[$nPointerCount]
   Local $lData[2] = [$pAddress]
	$lData[1] = DllCall("kernel32.dll", 'int', 'ReadProcessMemory', 'handle', $hProcess, 'ptr', $pAddress, $sType, 0, 'ulong_ptr', $iSize, 'ulong_ptr*', 0)[3]
   Return $lData
EndFunc

Func _GWCA_InjectDLL($hProcess,$sDLLPath)
	Static Local $tDLLPath = DllStructCreate('char[256]')
	DllCall("kernel32.dll", "DWORD", "GetFullPathNameA", "str", $sDLLPath, "DWORD", 255, "ptr", DllStructGetPtr($tDLLPath), "int", 0)
	$sDLLPath = DllStructGetData($tDLLPath,1)
	Local $iStrSz = StringLen($sDLLPath) + 1

	Local $hKernel = DllCall("kernel32.dll", "DWORD", "GetModuleHandleA", "str", "kernel32.dll")[0]

	Local $pLoadLib = DllCall("kernel32.dll", "DWORD", "GetProcAddress", "DWORD", $hKernel, "str", "LoadLibraryA")[0]

	Local $pBuffer = DllCall("kernel32.dll", "DWORD", "VirtualAllocEx", "int", $hProcess, "int", 0, "ULONG_PTR", $iStrSz, "DWORD", 0x3000, "int", 4)[0]

	_GWCA_Write($hProcess,$pBuffer,$tDLLPath,'STRUCT*',$iStrSz)

	Local $hThread = DllCall("kernel32.dll", "int", "CreateRemoteThread", "DWORD", $hProcess, "int", 0, "int", 0, "DWORD", $pLoadLib, "DWORD", $pBuffer, "int", 0, "int", 0)[0]

	DllCall("kernel32.dll", "int", "WaitForSingleObject", "HANDLE", $hThread, "DWORD", -1)

	DllCall("kernel32.dll", "DWORD", "VirtualFreeEx", "HANDLE", $hProcess, "PTR", $pBuffer, "ULONG_PTR", 0, "DWORD", 0x8000)

	Return DllCall("kernel32.dll", "int", "GetExitCodeThread", "HANDLE", $hThread, "DWORD*", 0)[2]
EndFunc

Func _GWCA_EjectDLL($hProcess,$hDLL)
	Local $hKernel = DllCall("kernel32.dll", "DWORD", "GetModuleHandleA", "str", "kernel32.dll")[0]

	Local $pLoadLib = DllCall("kernel32.dll", "DWORD", "GetProcAddress", "DWORD", $hKernel, "str", "FreeLibrary")[0]

	Local $hThread = DllCall("kernel32.dll", "int", "CreateRemoteThread", "DWORD", $hProcess, "int", 0, "int", 0, "DWORD", $pLoadLib, "DWORD", $hDLL, "int", 0, "int", 0)[0]

	DllCall("kernel32.dll", "int", "WaitForSingleObject", "HANDLE", $hThread, "DWORD", -1)

	Return DllCall("kernel32.dll", "int", "GetExitCodeThread", "HANDLE", $hThread, "DWORD*", 0)[2]
EndFunc

#EndRegion


#Region Communicator Base

Func _GWCA_NamedPipeConnect($iProcessId,$bAllowOtherConnections = False)
	Local $sPipeName = StringFormat('\\\\.\\pipe\\GWComm_%d',$iProcessId)

	Local $hPipe = DllCall('kernel32.dll', 'HANDLE', 'CreateFileW', 'WSTR', $sPipeName, 'DWORD', 0xC0000000, 'DWORD', $bAllowOtherConnections ? 0x3 : 0, 'PTR', 0, 'DWORD', 0x3, 'DWORD', 0x80, 'PTR', 0)[0]

	If $hPipe = -1 Then Return SetError(1,0,0)

	DllCall('kernel32.dll','BOOL','SetNamedPipeHandleState','HANDLE',$hPipe,'DWORD*',0x2,'DWORD*', 0, 'DWORD*', 0)

	Return SetError(0,0,$hPipe)
EndFunc

Func _GWCA_NamedPipeDisconnect($hPipe)
	_GWCA_NamedPipeSend($hPipe,$GWCA_DISCONNECT_PIPE_CONNECTION,'INT*',4)
	Return DllCall("kernel32.dll", "BOOL", "CloseHandle", "HANDLE", $hPipe)[0]
EndFunc

Func _GWCA_NamedPipeSend($hPipe,$vBuffer,$sType = 'STRUCT*', $iSize = DllStructGetSize($vBuffer))
	Local $aResult = DllCall('kernel32.dll', 'BOOL', 'WriteFile', 'HANDLE', $hPipe, $sType, $vBuffer, 'DWORD', $iSize, 'DWORD*', 0, 'PTR', 0)
	Return SetExtended($aResult[4],$aResult[0])
EndFunc

Func _GWCA_NamedPipeRecv($hPipe,$vBuffer,$sType = 'STRUCT*', $iSize = DllStructGetSize($vBuffer))
	Local $aResult = DllCall('kernel32.dll', 'BOOL', 'ReadFile', 'HANDLE', $hPipe, $sType, $vBuffer, 'DWORD', $iSize, 'DWORD*', 0, 'PTR', 0)
	Return SetExtended($aResult[4],$aResult[2])
EndFunc

Func _GWCA_NamedPipeTransact($hPipe, $vInBuffer = 0, $sInType = 'STRUCT*', $iInSize = DllStructGetSize($vInBuffer), $vOutBuffer = 0, $sOutType = 'STRUCT*', $iOutSize = DllStructGetSize($vOutBuffer))
	Local $aResult = DllCall('kernel32.dll','BOOL','TransactNamedPipe','HANDLE',$hPipe,$sInType,$vInBuffer,'DWORD',$iInSize,$sOutType,$vOutBuffer,'DWORD',$iOutSize,'DWORD*',0,'PTR',0)
	Return SetExtended($aResult[6],$aResult[4])
EndFunc

#EndRegion


#Region Commands

Func _GWCA_TravelGH($hPipe)
	Return _GWCA_NamedPipeSend($hPipe,$GWCA_TRAVEL_GUILD_HALL,'INT*',4)
EndFunc

Func _GWCA_UnloadServer($hPipe)
	Return _GWCA_NamedPipeSend($hPipe,$GWCA_UNLOAD_SERVER,'INT*',4)
EndFunc

Func _GWCA_SendChat($hPipe,$sMessage,$cChannel = '!')
	Static Local $tBuffer = DllStructCreate('int;wchar;wchar[137]')

	DllStructSetData($tBuffer,1,$GWCA_SEND_CHAT)
	DllStructSetData($tBuffer,2,$cChannel)
	DllStructSetData($tBuffer,3,$sMessage)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',280)
EndFunc

Func _GWCA_Dialog($hPipe,$iDialogId)
	Static Local $tBuffer = DllStructCreate('int;dword')

	DllStructSetData($tBuffer,1,$GWCA_SEND_DIALOG)
	DllStructSetData($tBuffer,2,$iDialogId)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',8)
EndFunc

Func _GWCA_Move($hPipe,$fX,$fY,$iZPlane = 0)
	Static Local $tBuffer = DllStructCreate('int;float;float;dword')

	DllStructSetData($tBuffer,1,$GWCA_MOVE)
	DllStructSetData($tBuffer,2,$fX)
	DllStructSetData($tBuffer,3,$fY)
	DllStructSetData($tBuffer,4,$iZPlane)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x10)
EndFunc

Func _GWCA_MapTravel($hPipe,$nMapID,$nDistrict = 0,$nRegion = 0,$nLanguage = 0)
	Static Local $tBuffer = DllStructCreate('int;dword;int;int;int')

	DllStructSetData($tBuffer,1,$GWCA_MAP_TRAVEL)
	DllStructSetData($tBuffer,2,$nMapID)
	DllStructSetData($tBuffer,3,$nDistrict)
	DllStructSetData($tBuffer,4,$nRegion)
	DllStructSetData($tBuffer,5,$nLanguage)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x14)
EndFunc

Func _GWCA_MapTravelRandom($hPipe,$nMapID)
	Static Local $tBuffer = DllStructCreate('int;dword')

	DllStructSetData($tBuffer,1,$GWCA_MAP_TRAVEL_RANDOM)
	DllStructSetData($tBuffer,2,$nMapID)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x8)
EndFunc

Func _GWCA_ChangeSecondary($hPipe,$nProfession,$nHeroIndex = 0)
	Static Local $tBuffer = DllStructCreate('int;dword;dword')

	DllStructSetData($tBuffer,1,$GWCA_CHANGE_SECONDARY)
	DllStructSetData($tBuffer,2,$nHeroIndex)
	DllStructSetData($tBuffer,3,$nProfession)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0xC)
EndFunc

Func _GWCA_SetAttributes($hPipe,$aAttributeArray,$nHeroIndex = 0)
	Static Local $tBuffer = DllStructCreate('int;dword;dword;dword[16];dword[16]')

	DllStructSetData($tBuffer,1,$GWCA_SET_ATTRIBUTES)
	DllStructSetData($tBuffer,2,$nHeroIndex)
	DllStructSetData($tBuffer,3,$aAttributeArray[0][0])

	For $i = 1 To $aAttributeArray[0][0]
		DllStructSetData($tBuffer,4,$aAttributeArray[$i][0],$i)
		DllStructSetData($tBuffer,5,$aAttributeArray[$i][1],$i)
	Next

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x8C)
EndFunc

Func _GWCA_LoadSkillbar($hPipe,$aSkills,$nHeroIndex = 0)
	Static Local $tBuffer = DllStructCreate('int;dword;dword[8]')

	DllStructSetData($tBuffer,1,$GWCA_LOAD_SKILLBAR)
	DllStructSetData($tBuffer,2,$nHeroIndex)

	For $i = 1 To $aSkills[0]
		DllStructSetData($tBuffer,3,$aSkills[$i],$i)
	Next

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x28)
EndFunc

Func _GWCA_SetRenderHook($hPipe,$bFlag = True)
	Static Local $tBuffer = DllStructCreate('int;dword')

	DllStructSetData($tBuffer,1,$GWCA_SET_RENDER_HOOK)
	DllStructSetData($tBuffer,2,$bFlag)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x8)
EndFunc

Func _GWCA_SetDifficulty($hPipe,$bHardMode = True)
	Static Local $tBuffer = DllStructCreate('int;dword;dword;dword')

	DllStructSetData($tBuffer,1,$GWCA_SEND_PACKET)
	DllStructSetData($tBuffer,2,0x8)
	DllStructSetData($tBuffer,3,0x95)
	DllStructSetData($tBuffer,4,$bHardMode)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',0x10)
EndFunc


#EndRegion


#Region Queries

Func _GWCA_ChangeTarget($hPipe, $Id)
     Static Local $tInBuffer = DllStructCreate('int;dword')
     DllStructSetData($tInBuffer, 1, $GWCA_CHANGE_TARGET)
     DllStructSetData($tInBuffer, 2, $Id)
     Return _GWCA_NamedPipeSend($hPipe, $tInBuffer,'STRUCT*',8)
EndFunc


;~ not a proper Agent struct, we ignore pointer/useless/unknown fields
;~ to minimize data transfer. Feel free to add/uncomment fields if needed.

;~ Description: Returns a Struct with
;~ long Id; //AgentId
;~ float Rotation; //Rotation in radians from East (-pi to pi)
;~ long NameProperties; //Bitmap basically telling what the agent is
;~ float X; //X coord in float
;~ float Y; //Y coord in float
;~ DWORD Ground;
;~ long Type; //0xDB = players, npc's, monsters etc. 0x200 = signpost/chest/object (unclickable). 0x400 = item to pick up
;~ float MoveX; //If moving, how much on the X axis per second
;~ float MoveY; //If moving, how much on the Y axis per second
;~ long ExtraType;
;~ float WeaponAttackSpeed; //The base attack speed in float of last attacks weapon. 1.33 = axe, sWORD, daggers etc.
;~ float AttackSpeedModifier; //Attack speed modifier of the last attack. 0.67 = 33% increase (1-.33)
;~ WORD PlayerNumber; //Selfexplanatory. All non-players have identifiers for their type. Two of the same mob = same number
;~ BYTE Primary; //Primary profession 0-10 (None,W,R,Mo,N,Me,E,A,Rt,P,D)
;~ BYTE Secondary; //Secondary profession 0-10 (None,W,R,Mo,N,Me,E,A,Rt,P,D)
;~ BYTE Level; //Duh!
;~ BYTE TeamId; //0=None, 1=Blue, 2=Red, 3=Yellow
;~ float Energy; //Only works for yourself
;~ long MaxEnergy; //Only works for yourself
;~ float HPPips; //Regen/degen as float
;~ float HP; //Health in % where 1=100% and 0=0%
;~ long MaxHP; //Only works for yourself
;~ long Effects; //Bitmap for effects to display when targetted. DOES include hexes
;~ BYTE Hex; //Bitmap for the hex effect when targetted (apparently obsolete!)
;~ long ModelState; //Different values for different states of the model.
;~ long TypeMap; //Odd variable! 0x08 = dead, 0xC00 = boss, 0x40000 = spirit, 0x400000 = player
;~ long InSpiritRange; //Tells if agent is within spirit range of you. Doesn't work anymore?
;~ long LoginNumber; //Unique number in instance that only works for players
;~ WORD Allegiance; //0x100 = ally/non-attackable, 0x300 = enemy, 0x400 = spirit/pet, 0x500 = minion, 0x600 = npc/minipet
;~ WORD WeaponType; //1=bow, 2=axe, 3=hammer, 4=daggers, 5=scythe, 6=spear, 7=sWORD, 10=wand, 12=staff, 14=staff
;~ WORD Skill; //0 = not using a skill. Anything else is the Id of that skill

;idk what i did wrok get not the right data !?
Func _GWCA_GetPlayer($hPipe)
	Static Local $tOutBuffer = DllStructCreate('long AgentID;' & _
	'float Rotation;' & _;		4	Pos 1
	'long NameProperties;' & _ ;		4	Pos 2
	'float X;' & _ ;			4	Pos 3
	'float Y;' & _;			4	Pos 4
	'long Type;' & _ ;			4	Pos 5
	'float MoveX;' & _ ;		4	Pos 6
	'float MoveY;' & _ ;		4	Pos 7
	'long ExtraTyp;' & _;		4	Pos 8 =32
	'WORD PlayerNumber;' & _ ;		2	Pos 9
	'BYTE Primary;' & _ ;		1	Pos 10
	'BYTE Secondary;' & _;		1	Pos 11
	'BYTE Level;' & _ ;			1	Pos 12
	'BYTE TeamId;' & _ ;		1	Pos 13
	'float Energy;' & _ ;		4	Pos 14 = 10
	'long MaxEnergy;' & _ ;		4	Pos 15
	'float HPPips;' & _ ;		4	Pos 16
	'float HP;' & _;			4	Pos 17
	'long MaxHP;' & _ ;			4	Pos 18
	'long Effects;' & _;		4	Pos 19 = 20
	'BYTE Hex;' & _;			1	Pos 20
	'long ModelState;' & _ ;		4	Pos 21
	'long TypeMap;' & _ ;		4	Pos 22
	'long InSpiritRange;' & _;		4	Pos 23
	'long LoginNumber;' & _ ;		4	Pos 24
	'WORD Allegianc;' & _ ;		2	Pos 26
	'WORD Skill');			2	Pos 27 = 21

	_GWCA_NamedPipeTransact($hPipe, $GWCA_GET_PLAYER,'DWORD*',4, $tOutBuffer,'STRUCT*', 83)
	Return $tOutBuffer
EndFunc
;~ Func _GWCA_GetPlayer($hPipe)
;~ 	Static Local $tInBuffer = DllStructCreate('int')
;~ 	Static Local $tOutBuffer = DllStructCreate('long AgentID;float Rotation;' & _
;~ 	'long NameProperties; float X;float Y;DWORD Ground;' & _
;~ 	'long Type;float MoveX;float MoveY;long ExtraTyp;float WeaponAttackSpeed;' & _
;~ 	'float AttackSpeedModifier;WORD PlayerNumber;BYTE Primary;BYTE Secondary;' & _
;~ 	'BYTE Level;BYTE TeamId;float Energy;long MaxEnergy;float HPPips;float HPlong;' & _
;~ '	MaxHPlong Effects;BYTE Hex;long ModelState;long TypeMap;long InSpiritRange;' & _
;~ 	'long LoginNumber;WORD Allegianc;WORD WeaponType;WORD Skill')
;~ 	DllStructSetData($tInBuffer, 1, $GWCA_GET_PLAYER)
;~ 	_GWCA_NamedPipeTransact($hPipe, $tInBuffer,'STRUCT*', 0x4, $tOutBuffer, 'STRUCT*', 0x66)
;~ 	Return $tOutBuffer
;~ EndFunc
;idk what i did wrok get not the right data !?
Func _GWCA_GetAgentByID($hPipe, $Id)
	Static Local $tInBuffer = DllStructCreate('int;dword')
	Static Local $tOutBuffer = DllStructCreate('long AgentID;' & _
	'float Rotation;' & _;		4
	'long NameProperties;' & _ ;		4
	'float X;' & _ ;			4
	'float Y;' & _;			4
	'long Type;' & _ ;			4
	'float MoveX;' & _ ;		4
	'float MoveY;' & _ ;		4
	'long ExtraTyp;' & _;		4
	'WORD PlayerNumber;' & _ ;		2
	'BYTE Primary;' & _ ;		1
	'BYTE Secondary;' & _;		1
	'BYTE Level;' & _ ;			1
	'BYTE TeamId;' & _ ;		1
	'float Energy;' & _ ;		4
	'long MaxEnergy;' & _ ;		4
	'float HPPips;' & _ ;		4
	'float HP;' & _;			4
	'long MaxHP;' & _ ;			4
	'long Effects;' & _;		4
	'BYTE Hex;' & _;			1
	'long ModelState;' & _ ;		4
	'long TypeMap;' & _ ;		4
	'long InSpiritRange;' & _;		4
	'long LoginNumber;' & _ ;		4
	'WORD Allegianc;' & _ ;		2
	'WORD Skill');			2
	DllStructSetData($tInBuffer, 1, $GWCA_GET_AGENT_BY_ID)
	DllStructSetData($tInBuffer, 2, $Id)
	_GWCA_NamedPipeTransact($hPipe, $tInBuffer,'STRUCT*', 8, $tOutBuffer, 'STRUCT*', 83)
	Return $tOutBuffer
EndFunc

;~ Description: Returns the time in milliseconds that the instance has been alive.e
Func _GWCA_GetInstanceTime($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe, $GWCA_GET_INSTANCE_TIME,'DWORD*',4,0,'DWORD*',4)
EndFunc

;~ Description: Returns the current instance type
;~ Return 0 Outpost
;~ Return 1 Explorable
Func _GWCA_InstanceType($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe, $GWCA_GET_INSTANCE_TYPE,'DWORD*',4,0,'DWORD*',4)
EndFunc

;~ Description: Returns a DllStruct with
;~ district
;~ region
;~ language
Func _GWCA_GetMapInfo($hPipe)
	Static Local $tOutBuffer = DllStructCreate('dword;dword;dword')
	_GWCA_NamedPipeTransact($hPipe, $GWCA_GET_MAP_INFO, 'DWORD*',4, $tOutBuffer, 'STRUCT*', 0xC)
	Return $tOutBuffer
EndFunc

;~ Description: Returns the State
;~ Return 0 Not loaded
;~ Return 1 loaded
Func _GWCA_GetMapIsLoaded($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe,$GWCA_GET_MAP_LOADED,'DWORD*',4,0,'DWORD*',4)
EndFunc

Func _GWCA_GetMapId($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe,$GWCA_GET_MAP_ID,'DWORD*',4,0,'DWORD*',4)
EndFunc

Func _GWCA_GetPlayerId($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe,$GWCA_GET_PLAYER_ID,'DWORD*',4,0,'DWORD*',4)
EndFunc

Func _GWCA_GetTargetId($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe,$GWCA_GET_TARGET_ID,'DWORD*',4,0,'DWORD*',4)
EndFunc

Func _GWCA_RequestBasePointers($hPipe)
	Return _GWCA_NamedPipeTransact($hPipe, $GWCA_REQUEST_BASE_POINTERS,'DWORD*',4,0,'DWORD*',4)
EndFunc


Func _GWCA_GetAgentPtr($hPipe,$nAgentId)
	Static Local $tInBuffer = DllStructCreate('int;int')
	Static Local $tOutBuffer = DllStructCreate('ptr')

	DllStructSetData($tInBuffer,1,$GWCA_GET_AGENT_PTR)
	DllStructSetData($tInBuffer,2,$nAgentId)

	_GWCA_NamedPipeTransact($hPipe,$tInBuffer,'STRUCT*',8,$tOutBuffer,'STRUCT*',4)

	Return DllStructGetData($tOutBuffer,1)
EndFunc

Func _GWCA_GetAgentArray($hPipe, $nType = 0x6DB, $nAllegiance = 0, $nEffects = 0, $fX = $NaN,$fY = $NaN, $fMaxDistance = 5000,$fMinDistance = 0,$vPlayerNumbers = 0)
	Static Local $tInBuffer = DllStructCreate('int;dword;dword;dword;float;float;float;float;dword;dword[16]')
	Static Local $tOutBuffer = DllStructCreate('ptr[256]')

	DllStructSetData($tInBuffer,1,$GWCA_GET_FILTERED_AGENT_ARRAY)
	DllStructSetData($tInBuffer,2,$nType)
	DllStructSetData($tInBuffer,3,$nAllegiance)
	DllStructSetData($tInBuffer,4,$nEffects)
	DllStructSetData($tInBuffer,5,$fX)
	DllStructSetData($tInBuffer,6,$fY)
	DllStructSetData($tInBuffer,7,$fMinDistance)
	DllStructSetData($tInBuffer,8,$fMaxDistance)

	Local $nCount
	If IsArray($vPlayerNumbers) Then
		$nCount = $vPlayerNumbers[0]
		DllStructSetData($tInBuffer,9,$vPlayerNumbers[0])
		For $i = 1 To $nCount
			DllStructSetData($tInBuffer,10,$vPlayerNumbers[$i],$i)
		Next
	ElseIf $vPlayerNumbers = 0 Then
		$nCount = 0
		DllStructSetData($tInBuffer,9,0)
	ElseIf IsInt($vPlayerNumbers) Then
		$nCount = 1
		DllStructSetData($tInBuffer,9,1)
		DllStructSetData($tInBuffer,10,$vPlayerNumbers,1)
	EndIf

	_GWCA_NamedPipeSend($hPipe,$tInBuffer,'STRUCT*',36 + 4 * $nCount)

	$nCount = _GWCA_NamedPipeRecv($hPipe,0,'DWORD*',4)
	_GWCA_NamedPipeRecv($hPipe,$tOutBuffer,'STRUCT*',4 * $nCount)

	Local $aAgents[$nCount+1] = [$nCount]

	For $i = 1 To $nCount
		$aAgents[$i] = DllStructGetData($tOutBuffer,1,$i)
	Next

	Return $aAgents
EndFunc

Func _GWCA_GetCharacterName($hPipe)
	Static Local $tInBuffer = DllStructCreate('int')
	Static Local $tOutBuffer = DllStructCreate('wchar[20]')

	DllStructSetData($tInBuffer,1,$GWCA_GET_CHARACTER_NAME)

	_GWCA_NamedPipeTransact($hPipe,$tInBuffer,'STRUCT*',4,$tOutBuffer,'STRUCT*',40)

	Return DllStructGetData($tOutBuffer,1)
EndFunc

Func _GWCA_GetItemBySlot($hPipe,$iBag,$iSlot)
	Static Local $tInBuffer = DllStructCreate('int;byte;byte')

	DllStructSetData($tInBuffer,1,$GWCA_GET_ITEM_BY_SLOT)
	DllStructSetData($tInBuffer,2,$iBag)
	DllStructSetData($tInBuffer,3,$iSlot)

	Return _GWCA_NamedPipeTransact($hPipe,$tInBuffer,'STRUCT*',0x6,0,'PTR*',4)
EndFunc

Func _GWCA_GetItemName($hPipe,$hItem)
	Static Local $tInBuffer = DllStructCreate('int;dword')
	Static Local $tOutBuffer = DllStructCreate('wchar[256]')

	DllStructSetData($tInBuffer,1,$GWCA_GET_ITEM_NAME)
	DllStructSetData($tInBuffer,2,$hItem)

	_GWCA_NamedPipeSend($hPipe,$tInBuffer,'STRUCT*',0x8)
	_GWCA_NamedPipeRecv($hPipe,$tOutBuffer,'STRUCT*',512)

	Return DllStructGetData($tOutBuffer,1)
EndFunc

Func _GWCA_GetPlayerSkillRecharge($hPipe,$iSlot)
	Static Local $tInBuffer = DllStructCreate('int;dword')

	DllStructSetData($tInBuffer,1,$GWCA_GET_SKILL_RECHARGE)
	DllStructSetData($tInBuffer,2,$iSlot-1)

	Return _GWCA_NamedPipeTransact($hPipe,$tInBuffer,'STRUCT*',8,0,'DWORD*',4)
EndFunc

Func _GWCA_GetEffectTimeRemaining($hPipe,$iSkillId)
	Static Local $tInBuffer = DllStructCreate('int;dword')

	DllStructSetData($tInBuffer,1,$GWCA_GET_EFFECT_TIME_REMAINING)
	DllStructSetData($tInBuffer,2,$iSkillId)

	Return _GWCA_NamedPipeTransact($hPipe,$tInBuffer,'STRUCT*',8,0,'INT*',4)
EndFunc

Func _GWCA_FixAgentPosition($hPipe,$iAgentId)
	Static Local $tInBuffer = DllStructCreate('int;dword')

	DllStructSetData($tInBuffer,1,$GWCA_FIX_AGENT_POSITION)
	DllStructSetData($tInBuffer,2,$iAgentId)

	Return _GWCA_NamedPipeSend($hPipe,$tBuffer,'STRUCT*',8)
EndFunc

#EndRegion